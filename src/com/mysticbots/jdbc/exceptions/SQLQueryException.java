/*
 * Copyright (C) 2016 S1LENT W@RRIOR
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.exceptions;

import java.sql.SQLDataException;

/**
 *
 * @author S1LENT W@RRIOR
 */
public class SQLQueryException extends SQLDataException {

    public SQLQueryException() {
    }

    public SQLQueryException(String reason) {
        super(reason);
    }

    public SQLQueryException(String reason, String SQLState) {
        super(reason, SQLState);
    }

    public SQLQueryException(String reason, String SQLState, int vendorCode) {
        super(reason, SQLState, vendorCode);
    }

    public SQLQueryException(Throwable cause) {
        super(cause);
    }

    public SQLQueryException(String reason, Throwable cause) {
        super(reason, cause);
    }

    public SQLQueryException(String reason, String SQLState, Throwable cause) {
        super(reason, SQLState, cause);
    }

    public SQLQueryException(String reason, String SQLState, int vendorCode, Throwable cause) {
        super(reason, SQLState, vendorCode, cause);
    }

}
