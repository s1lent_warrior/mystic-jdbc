/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * (Note: This Annotation is still in experimental phase!)<p>&nbsp;</p>
 * This annotation is used over fields or bean properties/attributes that are
 * actually JdbcEntities by themselves.<p>&nbsp;</p>The example usage of this annotation
 * is given below:
 * <pre><code>
 * &#064;JdbcEntity(tableName = "student")
 * public class StudentBean {
 *     &#064;JdbcAttribute(columnName = "s_id", isKeyForMap = true)
 *     int studentId;
 *     &#064;JdbcAttribute(columnName = "s_name")
 *     String studentName;
 *     &#064;JdbcAttributeObject(tableName = "degree", tableClass = DegreeBean.class, foreignKey = "d_id")
 * DegreeBean degree;
 * }
 *
 * &#064;JdbcEntity(tableName = "degree")
 * public class DegreeBean {
 *     &#064;JdbcAttribute(columnName = "d_id", isKeyForMap = true)
 *     int degreeId;
 *     &#064;JdbcAttribute(columnName = "d_name")
 *     String degreeTitle;
 * }</code></pre>
 *
 * @see JdbcEntity
 * @see JdbcAttribute
 * @author s1lent_warrior
 */
@Documented
@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface JdbcAttributeCollection {

    /**
     * The name of the table that has to be mapped with the annotated class.
     *
     * @return the table name
     */
    public String tableName();

    /**
     * The class of the annotated object that is mapped to the table
     *
     * @return the class of the object
     */
    public Class<?> tableClass();

    /**
     * The foreign key column name in the designated table. (optional)
     *
     * @return the name of foreign key column
     */
    public String foreignKey();
}
