/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is declared over classes that have to be mapped with a table
 * or entity in a database.<br>Important Note: The Class annotated with this
 * annotation must have a default no-arguments constructor.<br>The example usage
 * of this annotation is given below:
 * <pre><code>
 * &#064;JdbcEntity(tableName = "person")
 * public class PersonBean {
 *
 *     public PersonBean() {
 *     }
 *
 *     &#064;JdbcAttribute(columnName = "id", isKeyForMap = true)
 *     int id;
 *     &#064;JdbcAttribute(columnName = "name")
 *     String name;
 * }</code></pre>
 *
 * @author s1lent_warrior
 */
@Documented
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface JdbcEntity {

    /**
     * The name of the table that has to be mapped with the annotated class
     *
     * @return the table name
     */
    public String tableName();

    /**
     * The parent class of the annotated if its parent is also JdbcEntity
     * annotated.<br>The example usage of this optional attribute is given
     * below:
     * <pre><code>
     * &#064;JdbcEntity(tableName = "person")
     * public class PersonEntity {
     *     &#064;JdbcAttribute(columnName = "id", isKeyForMap = true)
     *     int id;
     *     &#064;JdbcAttribute(columnName = "name")
     *     String name;
     * }
     *
     * &#064;JdbcEntity(tableName = "person", parentEntityClass = PersonEntity.class)
     * public class StudentEntity extends PersonEntity {
     *     &#064;JdbcAttribute(columnName = "contact_number")
     *     String contactNumber;
     * }</code></pre>
     *
     * @return the class of parent entity
     */
    public Class<?> parentEntityClass() default JdbcEntity.class;
}
