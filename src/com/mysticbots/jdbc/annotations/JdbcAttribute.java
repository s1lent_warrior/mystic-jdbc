/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.annotations;

import com.mysticbots.jdbc.MysticJdbc;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used over fields or bean properties/attributes that needed
 * to be get from the database.
 *
 * @see JdbcEntity
 *
 * @author s1lent_warrior
 */
@Documented
@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface JdbcAttribute {

    /**
     * The name of the column in the database for the particular attribute.
     *
     * @return Column name in the database
     */
    public String columnName();

    /**
     * The name of the table for the particular attribute. This field is
     * optional and defaults to the {@link JdbcEntity#tableName} unless
     * specified.
     *
     * @return Table name for the particular attribute
     * @see JdbcEntity
     */
    public String tableName() default "";

    /**
     * Specifies weather this attribute is a key column. <br>Each
     * {@link JdbcEntity} must have at least 1 key. Otherwise some unexpected
     * behavior may happen!
     *
     * @return {@code true} if this attribute is a key else {@code false}
     *
     * @see MysticJdbc#getObjectsAsMapFromQuery(java.lang.String,
     * java.lang.Class, java.lang.Object[])
     * @see MysticJdbc#getObjectsAsMapFromResultSet(java.sql.ResultSet,
     * java.lang.Class)
     */
    public boolean isKeyColumn() default false;

    /**
     * Specifies weather this attribute is nullable in the database.
     *
     * @return {@code true} if this attribute is nullable else {@code false}
     */
    public boolean nullable() default false;

    /**
     * Specifies the default value of the field in database.
     *
     * @return Default value of the field in database
     */
    public String defaultValue() default "";
}
