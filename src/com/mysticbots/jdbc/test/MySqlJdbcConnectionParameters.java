/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test;

import com.mysticbots.jdbc.model.JdbcConnectionParameters;

/**
 * The sample implementation of <code>JdbcConnectionParameters</code> class
 *
 * @author S!LENT W@RRIOR
 * @version 1.0
 */
public class MySqlJdbcConnectionParameters extends JdbcConnectionParameters {

    /**
     * This is a static object that is advised to be used to avoid re-typing the
     * same code. Use this object in the constructor of classes that extend
     * <code>JdbcUtility</code> class like this:<br>
     * <code><br>
     * public class Test extends JdbcUtility {<br>
     * <br>
     * public Test() {<br>
     * super(MySqlJdbcConnectionParameters.jdbcParameters);<br>
     * }<br>
     * <br>
     *  // other methods and logic comes here<br>
     * }<br>
     * </code><br>
     *
     * @see JdbcConnectionParameters
     * @see com.mysticbots.jdbc.test.v2_0.TestNewJdbcUtilities
     */
    public static JdbcConnectionParameters jdbcParameters = new MySqlJdbcConnectionParameters();

    @Override
    public String getDatabaseName() {
        return "test_jdbc_utils";
    }

    @Override
    public String getUsername() {
        return "root";
    }

    @Override
    public String getPassword() {
        return "root";
    }

    @Override
    public String getDatabaseAddress() {
        return "localhost";
    }

    @Override
    public int getDatabasePort() {
        return 3306;
    }

    @Override
    public DatabaseType getDatabaseType() {
        return DatabaseType.MYSQL;
    }
}
