/*
 * Copyright (C) 2016 S1LENT W@RRIOR
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test.v2_0;

import com.mysticbots.jdbc.MysticJdbc;
import com.mysticbots.jdbc.test.beans.CourseBean;
import com.mysticbots.jdbc.test.beans.DegreeBean;
import com.mysticbots.jdbc.test.beans.DepartmentBean;
import com.mysticbots.jdbc.test.beans.FacultyBean;
import com.mysticbots.jdbc.test.beans.StudentBean;
import com.mysticbots.util.StackTracer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author S1LENT W@RRIOR
 */
public class Test {

    private static MysticJdbc jdbcUtilities = null;
//    private static final MysticJdbc jdbcUtilities
//            = new JdbcManager(MySqlJdbcConnectionParameters.jdbcParameters).jdbcUtilities;

    public static void insertRecords() throws Exception {
        TestNewJdbcUtilities test = new TestNewJdbcUtilities();
        try {
            // Insert Students in Database
            System.out.println("Inserting Students-------------------------------");
            int id = test.insertStudent("Abdullah", 8, 1);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Maaz", 6, 1);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Hammad", 4, 2);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Salman", 3, 2);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Abdul Malik", 1, 3);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Hamza", 5, 3);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Muneeb", 8, 1);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            System.out.println("-------------------------------------------------");
        } finally {
            // disconnect from database
            test.disconnect();
        }
    }

    public static List<StudentBean> getStudentsForDegree(int degreeId) throws Exception {
        String query = jdbcUtilities.queryBuilder.select(StudentBean.class).from()
                .where("degree_id = ?").build();
        List<StudentBean> students = jdbcUtilities.getObjectsAsListFromQuery(query,
                StudentBean.class, degreeId);
//        System.out.println(query);
        return students;
    }

    public static List<DegreeBean> getDegrees() throws Exception {
        String query = jdbcUtilities.queryBuilder.select("s, d, dd",
                StudentBean.class, DegreeBean.class, DepartmentBean.class)
                .from("student s JOIN degree d ON (d.degree_id = s.degree_id) "
                        + "JOIN department dd ON (d.department_id = dd.department_id)")
                .build();
        List<DegreeBean> degrees = jdbcUtilities.getObjectsAsListFromQuery(query, DegreeBean.class);
//        System.out.println(query);
        return degrees;
    }

    public static DegreeBean getDegreeWithId(int degreeId) throws Exception {
        String query = jdbcUtilities.queryBuilder.select("s, d, dd, c, f", StudentBean.class,
                DegreeBean.class, DepartmentBean.class, CourseBean.class, FacultyBean.class)
                .from("FROM student s JOIN degree d ON (d.degree_id = s.degree_id) "
                        + "JOIN department dd ON (d.department_id = dd.department_id) "
                        + "LEFT JOIN course_student cs ON (s.student_id = cs.student_id) "
                        + "LEFT JOIN course c ON (cs.course_id = c.course_id) "
                        + "JOIN faculty f ON (f.faculty_id = dd.director_id)")
                .where("d.degree_id = ?")
                .build();
        DegreeBean degree = jdbcUtilities.getJdbcEntityFromQuery(query, DegreeBean.class, degreeId);
//        System.out.println(query);
        return degree;
    }

    public static StudentBean getStudentWithId(int id) throws Exception {
        String query = jdbcUtilities.queryBuilder.select("s, c",
                StudentBean.class, CourseBean.class)
                .from("student s JOIN course_student cs ON (s.student_id = cs.student_id) "
                        + "JOIN course c ON (cs.course_id = c.course_id)")
                .where("s.student_id = ?").build();
//        System.out.println(query);
        return jdbcUtilities.getJdbcEntityFromQuery(query, StudentBean.class, id);
    }

    public static List<StudentBean> getStudents() throws Exception {
        String query = jdbcUtilities.queryBuilder.select("s, c",
                StudentBean.class, CourseBean.class)
                .from("student s JOIN course_student cs ON (s.student_id = cs.student_id) "
                        + "JOIN course c ON (cs.course_id = c.course_id)")
                .build();
        List<StudentBean> students = jdbcUtilities.getObjectsAsListFromQuery(query,
                StudentBean.class);
//        System.out.println(query);
        return students;
    }

    public static DepartmentBean getDepartmentWithId(int id) throws Exception {
        String query = jdbcUtilities.queryBuilder.select("dd, c, f",
                DepartmentBean.class, CourseBean.class, FacultyBean.class)
                .from("department dd  JOIN faculty f ON (f.faculty_id = dd.director_id) "
                        + "JOIN course c ON (f.faculty_id = c.faculty_id) ")
                .where("dd.department_id = ?")
                .build();
        DepartmentBean department = jdbcUtilities.getJdbcEntityFromQuery(query,
                DepartmentBean.class, id);
//        System.out.println(query);
        return department;
    }

    public static void main(String... args) throws Exception {

        String host = "jdbc:mysql://localhost/";
        String db = "ejb_1";
        String user = "root";
        String password = "root";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(host + db, user, password);

            String sqlQuery = "SELECT p.*, c.* FROM person p, clone c;";

            System.out.println("Query: " + conn.nativeSQL(sqlQuery) + "\n");

            ResultSet rs = conn.createStatement().executeQuery(sqlQuery);

            ResultSetMetaData md = rs.getMetaData();
            System.out.println("Table: " + md.getTableName(1));
            int cols = md.getColumnCount();
            System.out.printf("The query fetched %d columns\n", cols);
            System.out.println("These columns are: \n");

            for (int i = 1; i <= cols; i++) {

                String colName = md.getColumnName(i);

                String colAlias = md.getColumnLabel(i);

                String tableName = md.getTableName(i);

                System.out.println(colName + " AS " + tableName + "." + colAlias);

                System.out.println("---------------------------------\n");

            }

        } catch (ClassNotFoundException | SQLException e) {
            StackTracer.printStackTrace(e);
        }
//        Properties p = System.getProperties();
//        System.out.println(p.getProperty("user.home", "N/A"));
//        Test.insertRecords();
//        Test.getDegrees();
//        System.out.println(Test.getStudentsForDegree(1));
//        System.out.println(Test.getDegrees());
//        List<DegreeBean> degrees = Test.getDegrees();
//        System.out.println("Total Degrees: " + degrees.size());
//        for (DegreeBean degree : degrees) {
//            System.out.println("Total Students in " + degree.getDegreeTitle() + ": "
//                    + degree.getStudents().size());
//            System.out.println(degree.getStudents());
//        }
//        DegreeBean degree = Test.getDegreeWithId(1);
//        System.out.println(degree.getStudents());
//        for (StudentBean student : degree.getStudents()) {
//            System.out.println(student.getCourses());
//        }
////
//        System.out.println(degree);
//
//        System.out.println("Total Students in " + degree.getDegreeTitle() + ": "
//                    + degree.getStudents().size());
//        System.out.println(degree.getStudents());
//        StudentBean student = Test.getStudentWithId(1);
//        System.out.println(student.getCourses());
//        DepartmentBean department = Test.getDepartmentWithId(1);
//        System.out.println(department.getDirector().getCourses());

//        Properties p = System.getProperties();
//        System.out.println(p.getProperty("user.home", "N/A"));
//        Test.insertRecords();
//        Test.getDegrees();
//        System.out.println(Test.getStudentsForDegree(1));
//        System.out.println(Test.getDegrees());
//        List<DegreeBean> degrees = Test.getDegrees();
//        System.out.println("Total Degrees: " + degrees.size());
//        for (DegreeBean degree : degrees) {
//            System.out.println("Total Students in " + degree.getDegreeTitle() + ": "
//                    + degree.getStudents().size());
//            System.out.println(degree.getStudents());
//        }
//        DegreeBean degree = Test.getDegreeWithId(1);
//        System.out.println(degree.getStudents());
//        for (StudentBean student : degree.getStudents()) {
//            System.out.println(student.getCourses());
//        }
////        
//        System.out.println(degree);
//
//        System.out.println("Total Students in " + degree.getDegreeTitle() + ": "
//                    + degree.getStudents().size());
//        System.out.println(degree.getStudents());
//        StudentBean student = Test.getStudentWithId(1);
//        System.out.println(student.getCourses());
//        DepartmentBean department = Test.getDepartmentWithId(1);
//        System.out.println(department.getDirector().getCourses());
    }
}
