/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test.v2_0;

import com.mysticbots.jdbc.JdbcManager;
import com.mysticbots.jdbc.JdbcUtils;
import com.mysticbots.jdbc.MysticJdbc;
import com.mysticbots.jdbc.model.JdbcResultSetMetaData;
import com.mysticbots.jdbc.test.MySqlJdbcConnectionParameters;
import com.mysticbots.jdbc.test.beans.DegreeBean;
import com.mysticbots.jdbc.test.beans.StudentBean;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A sample test class to display the functionality of how-to-use this API
 *
 * @author S!LENT W@RRIOR
 * @version 2.0
 */
public class TestNewJdbcUtilities extends JdbcManager {

    /**
     * The default constructor. Should contain this statement:<br>
     * <code>super(MySqlJdbcConnectionParameters.jdbcParameters);</code><br> to
     * avoid re-typing the same code for database parameters!
     *
     * @see MySqlJdbcConnectionParameters#jdbcParameters
     */
    public TestNewJdbcUtilities() {
        super(MySqlJdbcConnectionParameters.jdbcParameters);
    }

    /**
     * Inserts a row into Degree table. Demonstrates the functionality of Insert
     * Operation
     *
     * @param degreeTitle The title of the Degree
     * @param degreeDescription The description of the Degree
     * @return The id of the row inserted
     * @throws Exception If an Exception occurred at Database Layer
     */
    public int insertDegree(String degreeTitle, String degreeDescription) throws Exception {
        String query = "INSERT INTO degree (degree_title, degree_description) VALUES (?, ?);";
        return this.jdbcUtilities.executeInsert(query, degreeTitle, degreeDescription);
    }

    /**
     * Updates the title of a Degree. This method demonstrates the functionality
     * of update .operation.
     *
     * @param degreeId The id of the degree
     * @param degree The new Degree data
     * @throws Exception If an Exception occurred at Database Layer
     * @see MysticJdbc#executeUpdate(String, Object[])
     */
    public void updateDegree(int degreeId, DegreeBean degree) throws Exception {
        String query = "UPDATE degree d "
                + "SET degree_title = ?, degree_description = ? "
                + "WHERE degree_id = ?;";
        this.jdbcUtilities.executeUpdate(query, degree.getDegreeTitle(), degree.getDegreeDescription(), degreeId);
    }

    /**
     * Deletes a row from degree table in database. This method demonstrates how
     * the delete operation can be performed.
     *
     * @param degreeId The id of the degree to be deleted
     * @throws Exception if SQLException occurs
     * @see MysticJdbc#deleteFromDatabase(String, String, Object)
     */
    public void deleteDegree(int degreeId) throws Exception {
        this.jdbcUtilities.deleteFromDatabase("degree", "degree_id", 10);
    }

    /**
     * Gets a single degree using the given student id from our sample database
     *
     * @param degreeId The id of the student to be returned
     * @return The Degree Object or null if no such student is found
     * @throws Exception If an Exception occurred at Database Layer
     * @see MysticJdbc#selectFromDatabase(String, Object[])
     */
    public DegreeBean getDegree(int degreeId) throws Exception {
        String query = "SELECT degree_id, degree_title, degree_description "
                + "FROM degree "
                + "WHERE degree_id = ?;";
        ResultSet rs = this.jdbcUtilities.selectFromDatabase(query, degreeId);
        return jdbcUtilities.getObjectFromResultSet(rs, DegreeBean.class);
        // alternatively you can directly get your object from query
        // return jdbcUtilities.getObjectFromQuery(query, DegreeBean.class, degreeId);
    }

    /**
     ** Method to get all degrees from our sample database
     *
     * @return List of Degrees
     * @throws Exception If an Exception occurred at Database Layer
     */
    public List<DegreeBean> getDegrees() throws Exception {
        List<DegreeBean> degrees = new ArrayList();
        String query = "SELECT degree_id, degree_title, degree_description "
                + "FROM degree;";
        ResultSet rs = this.jdbcUtilities.selectFromDatabase(query);
        int rowId = 1;
        while (rs.next()) {
            degrees.add(jdbcUtilities.getObjectFromResultSet(rs, DegreeBean.class, rowId++));
        }
        return degrees;
        // alternatively you can directly get your List from query or resutset
        // return jdbcUtilities.getObjectsAsListFromQuery(query, DegreeBean.class);
        // return jdbcUtilities.getObjectsAsListFromResultSet(rs, DegreeBean.class);
    }

    /**
     * Method to get all degrees as a {@code java.util.Map} from our sample
     * database
     *
     * @return Map of Degrees
     * @throws Exception if SQLException occurs
     */
    public Map<String, DegreeBean> getDegreesMap() throws Exception {
        String query = "SELECT degree_id, degree_title, degree_description "
                + "FROM degree;";
        ResultSet rs = this.jdbcUtilities.selectFromDatabase(query);
        return jdbcUtilities.getObjectsAsMapFromResultSet(rs, DegreeBean.class);
        // alternatively you can directly get your Map from query
        // return jdbcUtilities.getObjectsAsMapFromQuery(query, DegreeBean.class);
    }

    /**
     * Inserts a row into Student table. Demonstrates the functionality of
     * Insert Operation
     *
     * @param studentName The Name of the student
     * @param semester The semester of the student
     * @param degreeId The id of the Degree
     * @return The id of the row inserted
     * @throws Exception If an Exception occurred at Database Layer
     */
    public int insertStudent(String studentName, int semester, int degreeId) throws Exception {
        String query = "INSERT INTO student (student_name, semester, degree_id) VALUES (?, ?, ?);";
        return this.jdbcUtilities.executeInsert(query, studentName, semester, degreeId);
    }

    /**
     * Updates a row in student table in database. This method demonstrates the
     * functionality of update .operation using an updatable ResultSet.
     *
     * @param studentId Student Id
     * @param student New student data
     * @throws Exception If an Exception occurred at Database Layer
     * @see MysticJdbc#executeUpdate(String, Object[])
     * @see ResultSet#updateRow()
     */
    public void updateStudent(int studentId, StudentBean student) throws Exception {
        String query = "SELECT student_id, student_name, semester "
                + "FROM student "
                + "WHERE student_id = ?;";
        ResultSet rs = this.jdbcUtilities.selectFromDatabase(query, studentId);
        if (rs.next()) {
            rs.updateString(2, student.getStudentName());
            int nextSemester = rs.getInt(3) + 1;
            rs.updateInt(3, nextSemester);
            rs.updateRow();
        }
    }

    /**
     * Deletes a row from student table in database. This method demonstrates
     * how the delete operation can be performed.
     *
     * @param studentId The id of the student to be deleted
     * @throws Exception if SQLException occurs
     * @see MysticJdbc#deleteFromDatabase(String, String, Object)
     */
    public void deleteStudent(int studentId) throws Exception {
        this.jdbcUtilities.deleteFromDatabase("student", "student_id", studentId);
    }

    /**
     * Gets a single student using the given student id from our sample database
     *
     * @param studentId The id of the student to be returned
     * @return The Student Object or null if no such student is found
     * @throws Exception If an Exception occurred at Database Layer
     * @see MysticJdbc#selectFromDatabase(String, Object[])
     */
    public StudentBean getStudent(int studentId) throws Exception {
        String query = "SELECT student_id, student_name, semester, d.degree_id, degree_title, degree_description "
                + "FROM degree d JOIN student s ON (d.degree_id = s.degree_id) "
                + "WHERE student_id = ?;";
        ResultSet rs = jdbcUtilities.selectFromDatabase(query, studentId);
        return jdbcUtilities.getObjectFromResultSet(rs, StudentBean.class);
        // alternatively you can directly get your object from query
        // return jdbcUtilities.getObjectFromQuery(query, StudentBean.class, studentId);
    }

    /**
     * Method to get all students from our sample database
     *
     * @return List of Students
     * @throws Exception If an Exception occurred at Database Layer
     */
    public List<StudentBean> getStudents() throws Exception {
        String query = "SELECT student_id, student_name, semester, d.degree_id, degree_title, degree_description "
                + "FROM degree d JOIN student s ON (d.degree_id = s.degree_id);";
        ResultSet rs = jdbcUtilities.selectFromDatabase(query);
        return jdbcUtilities.getObjectsAsListFromResultSet(rs, StudentBean.class);
        // alternatively you can directly get your list from query
        // return jdbcUtilities.getObjectsAsListFromQuery(query, StudentBean.class);
    }

    /**
     * Method to get all students as a {@code java.util.Map} from our sample
     * database
     *
     * @return Map of Students
     * @throws Exception if SQLException occurs
     */
    public Map<Integer, StudentBean> getStudentsMap() throws Exception {
        String query = "SELECT student_id, student_name, semester, d.degree_id, degree_title, degree_description "
                + "FROM degree d JOIN student s ON (d.degree_id = s.degree_id);";
        ResultSet rs = jdbcUtilities.selectFromDatabase(query);
        return jdbcUtilities.getObjectsAsMapFromResultSet(rs, StudentBean.class);
        // alternatively you can directly get your Map from query
        // return jdbcUtilities.getObjectsAsMapFromQuery(query, StudentBean.class);
    }

    /**
     * Method to demonstrate the functionality to how to get and use the
     * ResultSet Meta Data.
     *
     * @return The resultSet for demonstrating CSV String feature
     * @throws Exception if SQLException occurs
     * @see JdbcUtils#getMetaDataForResultSet(ResultSet)
     */
    public ResultSet printResultSetMetaData() throws Exception {
        String query = "SELECT student_id AS 'Student Id', student_name AS 'Student Name', semester AS 'Current Semester', "
                + "d.degree_id AS 'Degree Id', degree_title AS 'Degree Title', degree_description AS 'Description'"
                + "FROM degree d JOIN student s ON (d.degree_id = s.degree_id);";
        ResultSet rs = this.jdbcUtilities.selectFromDatabase(query);
        JdbcResultSetMetaData metaData = JdbcUtils.getMetaDataForResultSet(rs);
        int columnCount = metaData.getColumnCount();
        int rowCount = metaData.getRowCount();
        System.out.println("Printing ResultSet MetaData-----------------------------------------");
        System.out.printf("Rows: %s\nColumns: %s\n", rowCount, columnCount);
        Set<String> columnNames = metaData.getColumns().keySet();
        System.out.printf("Original Column Names: %s \n", columnNames);
        Set<String> columnLabels = metaData.getColumns().keySet();
        System.out.printf("Column Labels: %s \n\n", columnLabels);
        return rs;
    }

    /**
     * A simple static function which inserts records into the Database
     *
     * @throws Exception If an Exception occurred at Database Layer
     */
    public static void insertRecords() throws Exception {
        TestNewJdbcUtilities test = new TestNewJdbcUtilities();
        try {
            // Insert Degrees in Database
            System.out.println("Inserting Degrees-------------------------------");
            int id = test.insertDegree("BIT", "Becholars in Information Technology");
            System.out.printf("New Degree inserted in database at Index: %s\n", id);
            id = test.insertDegree("BEE", "Becholars in Electrical Engineering");
            System.out.printf("New Degree inserted in database at Index: %s\n", id);
            id = test.insertDegree("BCS", "Becholars in Computer Sciences");
            System.out.printf("New Degree inserted in database at Index: %s\n", id);
            System.out.println("-------------------------------------------------");

            // Insert Students in Database
            System.out.println("Inserting Students-------------------------------");
            id = test.insertStudent("Abdullah", 8, 1);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Maaz", 6, 1);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Hammad", 4, 2);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Salman", 3, 2);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Abdul Malik", 1, 3);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Hamza", 5, 3);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            id = test.insertStudent("Muneeb", 8, 1);
            System.out.printf("New Student inserted in database at Index: %s\n", id);
            System.out.println("-------------------------------------------------");
        } finally {
            // disconnect from database
            test.disconnect();
        }
    }

    /**
     * Prints the contents of the given list to console
     *
     * @param list the list to be printed
     */
    public static void print(List list) {
        for (Object o : list) {
            System.out.println(o);
        }
        System.out.println("------------------------------------------\n");
    }

    /**
     * Prints the contents of the given map to console
     *
     * @param map the list to be printed
     */
    public static void print(Map map) {
        for (Iterator it = map.keySet().iterator(); it.hasNext();) {
            Object key = it.next();
            System.out.println(key + ": " + map.get(key));
        }
        System.out.println("------------------------------------------\n");
    }

    /**
     * The main method
     *
     * @param args the command line args
     * @throws Exception If an Exception occurred at Database Layer
     */
    public static void main(String... args) throws Exception {
        TestNewJdbcUtilities.insertRecords();  // Insert Records into database
        TestNewJdbcUtilities test = new TestNewJdbcUtilities();
        ResultSet resultSet = null;
        try {
            DegreeBean degree = test.getDegree(3);  // get a degree from database
            System.out.println(degree);

            degree.setDegreeTitle("BSE");   // modify degree bean values
            degree.setDegreeDescription("Becholars in Software Engineering");

            test.updateDegree(degree.getDegreeId(), degree);    // update degree
            System.out.println("Degree Updated!");

            test.deleteDegree(degree.getDegreeId());    // delete degree
            System.out.println("Degree Deleted!");

            List<DegreeBean> degrees = test.getDegrees();   // get all degrees
            System.out.println("Printing Degrees!-------------------------------");
            TestNewJdbcUtilities.print(degrees);   // print all degrees

            Map<String, DegreeBean> degreesMap = test.getDegreesMap(); // get degrees map
            System.out.println("Printing Degrees Map!-------------------------------");
            TestNewJdbcUtilities.print(degreesMap);   // print degrees map

            StudentBean student = test.getStudent(1);   // get a student from database
            System.out.println(student);

            student.setStudentName("Old Student");  // modify student bean values

            test.updateStudent(student.getStudentId(), student);    // update student
            System.out.println("Student Updated!");

            test.deleteStudent(6);    // delete student
            System.out.println("Student Deleted!");

            List<StudentBean> students = test.getStudents();   // get all students
            System.out.println("Printing Students!-------------------------------");
            TestNewJdbcUtilities.print(students);   // print all students

            Map<Integer, StudentBean> studentsMap = test.getStudentsMap(); // get students map
            System.out.println("Printing Students map!-------------------------------");
            TestNewJdbcUtilities.print(studentsMap);   // print students map

            resultSet = test.printResultSetMetaData(); // print resultSet MetaData and get resultSet object

            String csvString = JdbcUtils.toCsvString(resultSet, ';', true);  // get CSV String from resultset
            System.out.println("Printing CSV String--------------------------");
            System.out.println(csvString);

//            System.out.println("Saving ResultSet in CSV format--------------------------");
//            File file;
//            file = JdbcUtils.toCsvFile(resultSet, ',', true, "D:/output.csv"); // save resultSet to a file in CSV format
            // you can use either of these methods to write output in a csv format
//            file = test.toCsvFile(resultSet, ',', true, new File("D:/output.csv"));  
//            System.out.println("File Path: " + file.getAbsolutePath());

            /*
             * Uncomment below code to truncate student and degree tables
             */
            System.out.println("\nTruncating Student Table--------------------------");
            test.jdbcUtilities.truncate("student");

//            System.out.println("\nTruncating Degree Table--------------------------");
//            test.jdbcUtilities.truncate("degree");
        } finally {
            System.out.println("\nDisconnecting--------------------------");
            test.jdbcUtilities.disconnect(resultSet);
        }
        System.out.println("\nAll Done!");
    }
}
