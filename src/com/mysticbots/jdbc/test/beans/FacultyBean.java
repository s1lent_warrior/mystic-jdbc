/*
 * Copyright (C) 2016 S1LENT W@RRIOR
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test.beans;

import com.mysticbots.jdbc.annotations.JdbcAttribute;
import com.mysticbots.jdbc.annotations.JdbcAttributeCollection;
import com.mysticbots.jdbc.annotations.JdbcEntity;
import java.util.List;

/**
 *
 * @author S1LENT W@RRIOR
 */
@JdbcEntity(tableName = "faculty")
public class FacultyBean {

    @JdbcAttribute(columnName = "faculty_id", isKeyColumn = true)
    private int facultyId;
    @JdbcAttribute(columnName = "faculty_name")
    private String facultyName;
    @JdbcAttribute(columnName = "faculty_designation")
    private String facultyDesignation;
    @JdbcAttributeCollection(tableName = "course", tableClass = CourseBean.class,
            foreignKey = "course_id")
    List<CourseBean> courses;

    public int getFacultyId() {
        return facultyId;
    }

    public void setFacultyId(int facultyId) {
        this.facultyId = facultyId;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getFacultyDesignation() {
        return facultyDesignation;
    }

    public void setFacultyDesignation(String facultyDesignation) {
        this.facultyDesignation = facultyDesignation;
    }

    public List<CourseBean> getCourses() {
        return courses;
    }

    public void setCourses(List<CourseBean> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return String.format("FacultyBean{" + "facultyId = '%d', "
                + "facultyName = '%s', "
                + "facultyDesignation = '%s', "
                + "courses = '%s' }",
                facultyId, facultyName, facultyDesignation, courses);
    }
}
