/*
 * Copyright (C) 2016 S1LENT W@RRIOR
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test.beans;

import com.mysticbots.jdbc.annotations.JdbcEntity;

/**
 *
 * @author S1LENT W@RRIOR
 */
@JdbcEntity(tableName = "course_student", parentEntityClass = CourseBean.class)
public class StudentCourseBean extends CourseBean {

    private static final long serialVersionUID = 3170870698854223022L;

    private int semester;

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

}
