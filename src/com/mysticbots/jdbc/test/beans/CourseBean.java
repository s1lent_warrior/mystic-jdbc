/*
 * Copyright (C) 2016 S1LENT W@RRIOR
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test.beans;

import com.mysticbots.jdbc.annotations.JdbcAttribute;
import com.mysticbots.jdbc.annotations.JdbcEntity;
import java.io.Serializable;

/**
 *
 * @author S1LENT W@RRIOR
 */
@JdbcEntity(tableName = "course")
public class CourseBean implements Serializable {

    private static final long serialVersionUID = 6455621221263461102L;

    @JdbcAttribute(columnName = "course_id", isKeyColumn = true)
    private int courseId;

    @JdbcAttribute(columnName = "course_title")
    private String courseTitle;

    @JdbcAttribute(columnName = "course_credits")
    private int creditHours;

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public int getCreditHours() {
        return creditHours;
    }

    public void setCreditHours(int creditHours) {
        this.creditHours = creditHours;
    }

    @Override
    public String toString() {
        return String.format("CourseBean{" + "courseId = %d, "
                + "courseTitle = '%s', "
                + "creditHours = %d}",
                courseId, courseTitle, creditHours);
    }
}
