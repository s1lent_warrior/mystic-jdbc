/*
 * Copyright (C) 2016 S1LENT W@RRIOR
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test.beans;

import com.mysticbots.jdbc.annotations.JdbcAttribute;
import com.mysticbots.jdbc.annotations.JdbcAttributeObject;
import com.mysticbots.jdbc.annotations.JdbcEntity;
import java.io.Serializable;

/**
 *
 * @author S1LENT W@RRIOR
 */
@JdbcEntity(tableName = "department")
public class DepartmentBean implements Serializable {

    private static final long serialVersionUID = 1143998278938241329L;

    @JdbcAttribute(columnName = "department_id", isKeyColumn = true)
    private int departmentId;
    @JdbcAttribute(columnName = "department_name")
    private String departmentName;
    @JdbcAttribute(columnName = "department_description")
    private String departmentDescription;
    @JdbcAttributeObject(tableName = "faculty", foreignKey = "director_id")
    private FacultyBean director;

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentDescription() {
        return departmentDescription;
    }

    public void setDepartmentDescription(String departmentDescription) {
        this.departmentDescription = departmentDescription;
    }

    public FacultyBean getDirector() {
        return director;
    }

    public void setDirector(FacultyBean director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return String.format("DepartmentBean{ departmentId = %d, "
                + "departmentName = '%s', "
                + "departmentDescription = '%s', "
                + "director = '%s' }",
                departmentId, departmentName, departmentDescription, director);
    }
}
