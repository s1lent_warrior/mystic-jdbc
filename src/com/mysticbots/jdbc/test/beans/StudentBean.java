/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test.beans;

import com.mysticbots.jdbc.annotations.JdbcAttribute;
import com.mysticbots.jdbc.annotations.JdbcAttributeCollection;
import com.mysticbots.jdbc.annotations.JdbcEntity;
import java.util.Set;

/**
 *
 * @author S!LENT W@RRIOR
 */
@JdbcEntity(tableName = "student")
public class StudentBean {

    @JdbcAttribute(columnName = "student_id", isKeyColumn = true)
    private int studentId;
    @JdbcAttribute(columnName = "student_name")
    private String studentName;
    @JdbcAttribute(columnName = "degree_id")
    private int degreeId;

    @JdbcAttributeCollection(tableName = "course", tableClass = CourseBean.class,
            foreignKey = "course_id")
    private Set<CourseBean> courses;

    /**
     * The default constructor
     */
    public StudentBean() {
        this(0, "", 0);
    }

    /**
     * Another constructor!
     *
     * @param studentId the student id
     * @param studentName the student name
     * @param semester the semester number
     * @param degree Student's Degree
     */
    public StudentBean(int studentId, String studentName, int degreeId) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.degreeId = degreeId;
    }

    /**
     * Retrieves the student id
     *
     * @return the student id
     */
    public int getStudentId() {
        return studentId;
    }

    /**
     * Sets the id of the student
     *
     * @param studentId the new student id
     */
    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    /**
     * Gets the name of the student
     *
     * @return the student name
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * Sets the name of the student
     *
     * @param studentName the student name
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getDegreeId() {
        return degreeId;
    }

    public void setDegreeId(int degreeId) {
        this.degreeId = degreeId;
    }

    public Set<CourseBean> getCourses() {
        return courses;
    }

    public void setCourses(Set<CourseBean> courses) {
        this.courses = courses;
    }

    @Override
    public String toString() {
        return String.format("StudentBean { studentId = %d, "
                + "studentName = '%s' "
                + "degreeId = %d}", studentId, studentName, degreeId);
    }
}
