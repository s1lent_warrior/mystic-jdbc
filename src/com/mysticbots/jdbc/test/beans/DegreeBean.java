/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.test.beans;

import com.mysticbots.jdbc.annotations.JdbcAttribute;
import com.mysticbots.jdbc.annotations.JdbcAttributeCollection;
import com.mysticbots.jdbc.annotations.JdbcAttributeObject;
import com.mysticbots.jdbc.annotations.JdbcEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author S!LENT W@RRIOR
 */
@JdbcEntity(tableName = "degree")
public class DegreeBean implements Serializable {

    private static final long serialVersionUID = 5343017621838928132L;

    @JdbcAttribute(columnName = "degree_id", isKeyColumn = true)
    private int degreeId;
    @JdbcAttribute(columnName = "degree_title")
    private String degreeTitle;
    @JdbcAttribute(columnName = "degree_description")
    private String degreeDescription;
    @JdbcAttributeObject(tableName = "department", foreignKey = "department_id")
    private DepartmentBean department;
    @JdbcAttributeCollection(tableName = "student", tableClass = StudentBean.class,
            foreignKey = "student_id")
    private List<StudentBean> students;

    /**
     * The default Constructor
     */
    public DegreeBean() {
        this(0, "", "", null);
    }

    /**
     * Another Constructor!
     *
     * @param degreeId the degree id
     * @param degreeTitle the degree name
     * @param degreeDescription degree description
     */
    public DegreeBean(int degreeId, String degreeTitle, String degreeDescription) {
        this(degreeId, degreeTitle, degreeDescription, null);
    }

    /**
     * Another Constructor!
     *
     * @param degreeId the degree id
     * @param degreeTitle the degree name
     * @param degreeDescription degree description
     */
    public DegreeBean(int degreeId, String degreeTitle, String degreeDescription,
            List<StudentBean> students) {
        this.degreeId = degreeId;
        this.degreeTitle = degreeTitle;
        this.degreeDescription = degreeDescription;
        this.students = (students != null) ? students : new ArrayList();
    }

    /**
     * Sets the degree id
     *
     * @param degreeId new degree id
     */
    public void setDegreeId(int degreeId) {
        this.degreeId = degreeId;
    }

    /**
     * Sets the name of the degree
     *
     * @param degreeTitle the new name of degree
     */
    public void setDegreeTitle(String degreeTitle) {
        this.degreeTitle = degreeTitle;
    }

    /**
     * Sets some brief description about the degree
     *
     * @param degreeDescription degree description
     */
    public void setDegreeDescription(String degreeDescription) {
        this.degreeDescription = degreeDescription;
    }

    /**
     * Retrieves the degree id for a degree
     *
     * @return the degree id
     */
    public int getDegreeId() {
        return degreeId;
    }

    /**
     * Gets the name of the degree
     *
     * @return the degree name
     */
    public String getDegreeTitle() {
        return degreeTitle;
    }

    /**
     * Gives some brief description about the degree
     *
     * @return the degree description
     */
    public String getDegreeDescription() {
        return degreeDescription;
    }

    public DepartmentBean getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentBean department) {
        this.department = department;
    }

    public List<StudentBean> getStudents() {
        return students;
    }

    public void setStudents(List<StudentBean> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        String deptName = (department != null) ? department.getDepartmentName() : "";
        int studentSize = (students != null) ? students.size() : 0;

        return String.format("DegreeBean { degreeId = %d, "
                + "degreeTitle = '%s', "
                + "degreeDescription = '%s', "
                + "department = '%s', "
                + "students = %d }",
                degreeId, degreeTitle, degreeDescription, deptName, studentSize);
    }
}
