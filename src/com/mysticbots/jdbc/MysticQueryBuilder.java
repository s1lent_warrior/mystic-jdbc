/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc;

import com.mysticbots.jdbc.annotations.JdbcAttribute;
import com.mysticbots.jdbc.annotations.JdbcAttributeObject;
import com.mysticbots.jdbc.annotations.JdbcEntity;
import com.mysticbots.jdbc.exceptions.AnnotationNotFoundException;
import com.mysticbots.jdbc.exceptions.SQLQueryException;
import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author S1LENT W@RRIOR
 */
public class MysticQueryBuilder {

    private static final String QUERY_INSERT = "INSERT INTO %s (%s) \n%s;";

    private Map<String, Class> aliasesMap;

    private String select;
    private String from;
    private String where;
    private String groupBy;
    private String orderBy;
    private String limit;
    private String offset;

    private final MysticJdbc jdbcUtilities;

    MysticQueryBuilder(MysticJdbc jdbcUtilities) {
        this.jdbcUtilities = jdbcUtilities;
    }

    public MysticQueryBuilder select(Class... classes)
            throws AnnotationNotFoundException, NullPointerException {
        return this.select(new String[]{}, new String[]{}, classes);
    }

    public MysticQueryBuilder select(String aliases, Class... classes)
            throws AnnotationNotFoundException, NullPointerException {
        String[] aliasesArray = MysticQueryBuilder.getArrayFromString(aliases);

        return this.select(aliasesArray, null, classes);
    }

    public MysticQueryBuilder select(String aliases, String exclude, Class... classes)
            throws AnnotationNotFoundException, NullPointerException {
        String[] aliasesArray = MysticQueryBuilder.getArrayFromString(aliases);
        String[] excludeArray = MysticQueryBuilder.getArrayFromString(exclude);

        return this.select(aliasesArray, excludeArray, classes);
    }

    public MysticQueryBuilder select(String[] exclude, Class... classes)
            throws AnnotationNotFoundException, NullPointerException {
        return this.select(null, exclude, classes);
    }

    public MysticQueryBuilder select(String[] aliases, String[] exclude, Class... classes)
            throws AnnotationNotFoundException, NullPointerException {

        MysticQueryBuilder.validateClasses(classes);

        List<String> excludedColumns = new ArrayList();
        if (exclude != null) {
            excludedColumns = Arrays.asList(exclude);
        }

        aliasesMap = MysticQueryBuilder.mapClassesWithAliases(classes, aliases);

        StringBuilder query = new StringBuilder("SELECT");
        for (Map.Entry<String, Class> entrySet : aliasesMap.entrySet()) {
            String alias = entrySet.getKey();
            Class clazz = entrySet.getValue();

            int columnCount = 0;
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                JdbcAttributeObject entityObject = field.getAnnotation(JdbcAttributeObject.class);
                if (entityObject == null) { // plain object
                    JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
                    if (attribute != null && !excludedColumns.contains(attribute.columnName())) {
                        columnCount++;
                        String title = alias + "." + attribute.columnName();
                        query.append(" ");
                        query.append(title);
                        query.append(",");
                    }
                }
            }

            // return null if there are no coulmns annotated with JdbcAttribute annotation
            if (columnCount == 0) {
                throw new AnnotationNotFoundException("No properties annotated with JdbcAttribute "
                        + "annotation found on class " + clazz.getName()
                        + "\nIf this is not the case then check the excluded columns array");
            }
        }

        query.deleteCharAt(query.length() - 1);

        this.select = query.toString().trim().replaceAll(";", "");
//        this.select += "\n";

        return this;
    }

    public MysticQueryBuilder from() throws NullPointerException, AnnotationNotFoundException {
        String fromClause = this.getFromClauseForClasses(aliasesMap);
        return this.from(fromClause);
    }

    public MysticQueryBuilder from(String from)
            throws NullPointerException, AnnotationNotFoundException {
        if (from == null || from.isEmpty()) {
            from = this.getFromClauseForClasses(aliasesMap);
        }

        this.from = this.createClause("FROM", from);

        return this;
    }

    public MysticQueryBuilder where(String where) {
        this.where = this.createClause("WHERE", where);
        return this;
    }

    public MysticQueryBuilder groupBy(String groupBy) {
        this.groupBy = this.createClause("GROUP BY", groupBy);
        return this;
    }

    public MysticQueryBuilder orderBy(String orderBy) {
        this.orderBy = this.createClause("ORDER BY", orderBy);
        return this;
    }

    public MysticQueryBuilder limit(int limit) {
        String subquery = Integer.toString(limit);
        this.limit = this.createClause("LIMIT", subquery);
        return this;
    }

    public MysticQueryBuilder offset(int offset) {
        String subquery = Integer.toString(offset);
        this.offset = this.createClause("OFFSET", subquery);
        return this;
    }

    public String build()
            throws SQLException, NullPointerException, AnnotationNotFoundException {
        String message = null;

        if (this.select == null || this.select.isEmpty()) {
            // SELECT not present
            message = "Query without SELECT clause not allowed!";
        }

        if (this.from == null || this.from.isEmpty()) {
            // try to create FROM clause again
            this.from();

            if (this.from == null || this.from.isEmpty()) {
                if (message == null) {  // SELECT is present but FROM not present
                    message = "Query without SELECT clause not allowed!";
                } else {    // both SELECT and FROM not present
                    message = "Query without SELECT and FROM clause not allowed!";
                }
            }
        }

        if (message != null) {
            throw new SQLQueryException(message);
        }

        StringBuilder query = new StringBuilder();

        query.append(this.select);
        query.append('\n');

        query.append(this.from);
        query.append('\n');

        if (this.where != null && !this.where.isEmpty()) {
            query.append(this.where);
            query.append('\n');
        }

        if (this.groupBy != null && !this.groupBy.isEmpty()) {
            query.append(this.groupBy);
            query.append('\n');
        }

        if (this.orderBy != null && !this.orderBy.isEmpty()) {
            query.append(this.orderBy);
            query.append('\n');
        }

        if (this.limit != null && !this.limit.isEmpty()) {
            query.append(this.limit);
            query.append('\n');
        }

        if (this.offset != null && !this.offset.isEmpty()) {
            if (this.limit == null || this.limit.isEmpty()) {
                throw new SQLQueryException("OFFSET without LIMIT not allowed! ");
            }
            query.append(this.offset);
            query.append('\n');
        }

        query.deleteCharAt(query.length() - 1);

        String finalQuery = query.append(";").toString();

        this.select = null;
        this.from = null;
        this.where = null;
        this.groupBy = null;
        this.orderBy = null;
        this.limit = null;
        this.offset = null;

        return finalQuery;
    }

    public PreparedStatement prepare(Object... values)
            throws AnnotationNotFoundException, NullPointerException, SQLException {
        return this.jdbcUtilities.prepareStatement(this.build(), values);
    }

    private String createClause(String clause, String subquery) {
        StringBuilder query = new StringBuilder();
        if (subquery != null && !subquery.isEmpty()) {
            if (!subquery.startsWith(clause)) {
                query.append(clause.toUpperCase().trim());
                query.append(" ");
            }

            query.append(subquery.replaceAll(";", ""));

//            if (!subquery.endsWith("\n")) {
//                query.append("\n");
//            }
        }
        return query.toString();
    }

    private static Map<String, Class> mapClassesWithAliases(Class[] classes, String[] aliases)
            throws ArrayIndexOutOfBoundsException {

        List<String> aliasesList = new ArrayList(classes.length);
        if (aliases == null || aliases.length == 0) {
            for (Class clazz : classes) {
                JdbcEntity entity = (JdbcEntity) clazz.getAnnotation(JdbcEntity.class);
                aliasesList.add(entity.tableName());
            }
        } else {
            aliasesList = Arrays.asList(aliases);
        }

        if (classes.length != aliasesList.size()) {
            throw new ArrayIndexOutOfBoundsException("Length of classes and aliases mismatch!");
        }

        Map<String, Class> classesMap = new HashMap();
        for (int i = 0; i < classes.length; i++) {
            classesMap.put(aliasesList.get(i), classes[i]);
        }

        return classesMap;
    }

    private String getFromClauseForClasses(Map<String, Class> classes)
            throws NullPointerException, AnnotationNotFoundException {

        MysticQueryBuilder.validateClasses(classes.values().toArray(new Class[classes.size()]));

        StringBuilder sb = new StringBuilder("FROM");
        for (Map.Entry<String, Class> entrySet : classes.entrySet()) {
            String alias = entrySet.getKey();
            Class clazz = entrySet.getValue();
            JdbcEntity entity = (JdbcEntity) clazz.getAnnotation(JdbcEntity.class);
            sb.append(" ");
            sb.append(entity.tableName());

            if (!entity.tableName().equalsIgnoreCase(alias)) {
                sb.append(" ");
                sb.append(alias);
            }

            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);

        return sb.toString();
    }

    public static void validateClasses(Class... classes)
            throws AnnotationNotFoundException, NullPointerException {
        if (classes == null || classes.length == 0) {
            throw new NullPointerException("No Classes Available!");
        }

        for (Class clazz : classes) {
            if (clazz.getAnnotation(JdbcEntity.class) == null) {
                throw new AnnotationNotFoundException("JdbcEntity annotation not found on class "
                        + clazz.getName());
            }
        }
    }

    static String[] getArrayFromString(String csvString) {
        String[] aliasesArray = null;
        if (csvString != null && !csvString.isEmpty()) {
            aliasesArray = csvString.split(",");
            for (int i = 0; i < aliasesArray.length; i++) {
                aliasesArray[i] = aliasesArray[i].trim();
            }
        }
        return aliasesArray;
    }

    String getInsertQuery(Class clazz, int insertCount, String... exclude)
            throws AnnotationNotFoundException {

        String columns = this.getColumnNamesForInsert(clazz, exclude);

        String values = this.getValuesCluaseForInsert(insertCount, columns.split(","));

        JdbcEntity entity = (JdbcEntity) clazz.getAnnotation(JdbcEntity.class);

        String query = String.format(QUERY_INSERT, entity.tableName(), columns.trim(),
                values.trim());

        query = query.replaceAll(";", "");

        return query + ";";
    }

    <T> List<Object> getObjectsForInsert(String[] exclude, T... objects)
            throws IllegalAccessException, IllegalArgumentException {
        List<String> excludedColumns = Arrays.asList(exclude);

        List<Object> values = new ArrayList();
        Class clazz = objects[0].getClass();
        for (T object : objects) {
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                JdbcAttributeObject entityObject = field.getAnnotation(JdbcAttributeObject.class);
                if (entityObject == null) { // plain object
                    JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
                    if (attribute != null && !excludedColumns.contains(attribute.columnName())) {
                        Object value = field.get(object);
                        if (value == null && !attribute.defaultValue().isEmpty()) {
                            value = attribute.defaultValue();
                        }

                        values.add(value);
                    }
                }
            }
        }

        return values;
    }

    private String getColumnNamesForInsert(Class clazz, String... exclude)
            throws AnnotationNotFoundException {
        StringBuilder sb = new StringBuilder();

        List<String> excludedColumns = Arrays.asList(exclude);
        int columnCount = 0;
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            JdbcAttributeObject entityObject = field.getAnnotation(JdbcAttributeObject.class);
            if (entityObject == null) { // plain object
                JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
                if (attribute != null && !excludedColumns.contains(attribute.columnName())) {
                    columnCount++;
                    sb.append(" ");
                    sb.append(attribute.columnName());
                    sb.append(",");
                }
            }
        }

        // return null if there are no coulmns annotated with JdbcAttribute annotation
        if (columnCount == 0) {
            throw new AnnotationNotFoundException("No properties annotated with JdbcAttribute "
                    + "annotation found on class " + clazz.getName()
                    + "\nIf this is not the case then check the excluded columns array");
        }

        sb.deleteCharAt(sb.length() - 1);

        return sb.toString().trim();
    }

    private String getValuesCluaseForInsert(int insertCount, String... columns) {
        StringBuilder sb = new StringBuilder("VALUES\n");

        for (int j = 1; j <= insertCount; j++) {
            sb.append('(');
            for (int i = 0; i < columns.length; i++) {
                if (i != 0) {   // first iteration
                    sb.append(" ");
                }

                sb.append("?");

                if (i != columns.length - 1) {  // last iteration
                    sb.append(",");
                }
            }

            sb.append(')');

            if (j < insertCount) { // last iteration
                sb.append(",\n");
            }
        }

        return sb.toString();
    }

    public Map<Integer, String> getAllJdbcTypeNames() {

        Map<Integer, String> result = new HashMap();

        for (Field field : Types.class.getFields()) {
            try {
                String name = field.getName();
                result.put((Integer) field.getInt(name), name);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(MysticQueryBuilder.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(MysticQueryBuilder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return result;
    }

    public static String getSelectClause(Class... clazz)
            throws NullPointerException, AnnotationNotFoundException {
        return MysticQueryBuilder.getSelectClause(new String[]{}, new String[]{}, clazz);
    }

    public static String getSelectClause(String aliases, Class... classes)
            throws NullPointerException, AnnotationNotFoundException {
        String[] aliasesArray = MysticQueryBuilder.getArrayFromString(aliases);

        return MysticQueryBuilder.getSelectClause(aliasesArray, null, classes);
    }

    public static String getSelectClause(String aliases, String exclude, Class... classes)
            throws NullPointerException, AnnotationNotFoundException {
        String[] aliasesArray = MysticQueryBuilder.getArrayFromString(aliases);
        String[] excludeArray = MysticQueryBuilder.getArrayFromString(exclude);

        return MysticQueryBuilder.getSelectClause(aliasesArray, excludeArray, classes);
    }

    public static String getSelectClause(String[] exclude, Class... classes)
            throws NullPointerException, AnnotationNotFoundException {
        return MysticQueryBuilder.getSelectClause(null, exclude, classes);
    }

    public static String getSelectClause(String[] aliases, String[] exclude, Class... classes)
            throws NullPointerException, AnnotationNotFoundException {
        MysticQueryBuilder.validateClasses(classes);

        List<String> excludedColumns = new ArrayList();
        if (exclude != null) {
            excludedColumns = Arrays.asList(exclude);
        }

        Map<String, Class> classesMap = MysticQueryBuilder.mapClassesWithAliases(classes, aliases);

        StringBuilder sb = new StringBuilder("SELECT");
        for (Map.Entry<String, Class> entrySet : classesMap.entrySet()) {
            String alias = entrySet.getKey();
            Class clazz = entrySet.getValue();

            int columnCount = 0;
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                JdbcAttributeObject entityObject = field.getAnnotation(JdbcAttributeObject.class);
                if (entityObject == null) { // plain object
                    JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
                    if (attribute != null && !excludedColumns.contains(attribute.columnName())) {
                        columnCount++;
                        String title = alias + "." + attribute.columnName();
                        sb.append(" ");
                        sb.append(title);
                        sb.append(",");
                    }
                }
            }

            // return null if there are no coulmns annotated with JdbcAttribute annotation
            if (columnCount == 0) {
                throw new AnnotationNotFoundException("No properties annotated with JdbcAttribute "
                        + "annotation found on class " + clazz.getName()
                        + "\nIf this is not the case then check the excluded columns array");
            }
        }

        sb.deleteCharAt(sb.length() - 1);

        String query = sb.toString().trim().replaceAll(";", "");

        return query + "\n";
    }

    public static void main(String... args) throws Exception {

        String value = null;
        if (value == null || value.isEmpty()) {
            value = "0";
        }

        System.out.println(Float.parseFloat(value));

        String sqlType = "time".toUpperCase();

        int lengthIndexFrom = sqlType.indexOf("(");

        String name;
        int length = 0;
        int precision = 0;
        boolean unsigned = false;

        // name of sql type
        if (lengthIndexFrom == -1) {
            name = sqlType;
        } else {
            name = sqlType.substring(0, lengthIndexFrom);
        }

        int lengthIndexTo = sqlType.indexOf(")");

        if (lengthIndexFrom != -1 && !name.equalsIgnoreCase("ENUM")) {
            String lengthAndPrecision = sqlType.substring(lengthIndexFrom + 1,
                    lengthIndexTo);
            if (lengthAndPrecision.contains(",")) { // length and precision both exist
                String[] split = lengthAndPrecision.split(",");
                length = Integer.parseInt(split[0]);
                precision = Integer.parseInt(split[1]);
            } else {    // only length exist
                length = Integer.parseInt(lengthAndPrecision);
                precision = 0;
            }
        }

        // is unsigned
        if (!name.equalsIgnoreCase("ENUM")) {
            unsigned = sqlType.contains("UNSIGNED");
        }

        System.out.println("Name:      " + name);
        System.out.println("Length:    " + length);
        System.out.println("Precision: " + precision);
        System.out.println("Unsigned:  " + unsigned);
    }
}
