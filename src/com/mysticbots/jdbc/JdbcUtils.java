/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc;

import com.mysticbots.jdbc.model.JdbcResultSetMetaData;
import com.mysticbots.jdbc.model.JdbcResultSetMetaDataColumn;
import com.mysticbots.util.StackTracer;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class contains some Constants and static methods that are needed to
 * perform various tasks.
 *
 * @author S1LENT W@RRIOR
 */
public class JdbcUtils {

    /**
     * A Map containing Java equivalents of SQL Data Types as keys and their
     * corresponding getXXX methods from {@code ResultSet} class.
     */
    public static final Map<String, Method> MAP_TYPES = new HashMap();

    /**
     * A Map containing the java primitive data types and their corresponding
     * wrapper classes.
     */
    public static final Map<Class<?>, Class<?>> MAP_WRAPPERS = new HashMap();

    /**
     * A Map containing java primitive data type's (and their wrapper's) classes
     * as keys and their default values as values.<p>
     * &nbsp;</p>This Map was needed to overcome Exceptions where the database
     * returns null.
     */
    public static final Map<Class<?>, Object> MAP_DEFAULT_VALUES = new HashMap();

    /**
     * A Map containing SQL data types as keys and their equivalent java data
     * types as values
     */
    public static final Map<String, String[]> MAP_TYPES_SQL_JAVA = new HashMap();

    static {

        // SQL TO Java Types Map
        MAP_TYPES_SQL_JAVA.put("BIT", new String[]{"boolean", "byte[]"});
        MAP_TYPES_SQL_JAVA.put("BOOL", new String[]{"boolean", "int"});
        MAP_TYPES_SQL_JAVA.put("BOOLEAN", new String[]{"boolean", "int"});
        MAP_TYPES_SQL_JAVA.put("TINYINT", new String[]{"boolean", "int"});
        MAP_TYPES_SQL_JAVA.put("SAMLLINT", new String[]{"short", "int"});
        MAP_TYPES_SQL_JAVA.put("MEDIUMINT", new String[]{"int", "int"});
        MAP_TYPES_SQL_JAVA.put("INT", new String[]{"int", "long"});
        MAP_TYPES_SQL_JAVA.put("INTEGER", new String[]{"int", "long"});
        MAP_TYPES_SQL_JAVA.put("BIGINT", new String[]{"long", "java.math.BigInteger"});
        MAP_TYPES_SQL_JAVA.put("REAL", new String[]{"float", "double"});
        MAP_TYPES_SQL_JAVA.put("FLOAT", new String[]{"float", "double"});
        MAP_TYPES_SQL_JAVA.put("DOUBLE", new String[]{"double", "java.math.BigDecimal"});
        MAP_TYPES_SQL_JAVA.put("NUMERIC", new String[]{"java.math.BigDecimal",
            "java.math.BigDecimal"});
        MAP_TYPES_SQL_JAVA.put("DEC", new String[]{"java.math.BigDecimal",
            "java.math.BigDecimal"});
        MAP_TYPES_SQL_JAVA.put("DECIMAL", new String[]{"java.math.BigDecimal",
            "java.math.BigDecimal"});

        MAP_TYPES_SQL_JAVA.put("CHAR", new String[]{"String"});
        MAP_TYPES_SQL_JAVA.put("VARCHAR", new String[]{"String"});
        MAP_TYPES_SQL_JAVA.put("TINYTEXT", new String[]{"String"});
        MAP_TYPES_SQL_JAVA.put("TEXT", new String[]{"String"});
        MAP_TYPES_SQL_JAVA.put("MEDIUMTEXT", new String[]{"String"});
        MAP_TYPES_SQL_JAVA.put("LONGTEXT", new String[]{"String"});
        MAP_TYPES_SQL_JAVA.put("SET", new String[]{"String"});
        MAP_TYPES_SQL_JAVA.put("ENUM", new String[]{"String"});

        MAP_TYPES_SQL_JAVA.put("BINARY", new String[]{"byte[]"});
        MAP_TYPES_SQL_JAVA.put("VARBINARY", new String[]{"byte[]"});
        MAP_TYPES_SQL_JAVA.put("TINYBLOB", new String[]{"byte[]"});
        MAP_TYPES_SQL_JAVA.put("MEDIUMBLOB", new String[]{"byte[]"});
        MAP_TYPES_SQL_JAVA.put("BLOB", new String[]{"byte[]"});

        MAP_TYPES_SQL_JAVA.put("DATE", new String[]{"java.sql.Date"});
        MAP_TYPES_SQL_JAVA.put("TIME", new String[]{"java.sql.Time"});
        MAP_TYPES_SQL_JAVA.put("DATETIME", new String[]{"java.sql.Timestamp"});
        MAP_TYPES_SQL_JAVA.put("TIMESTAMP", new String[]{"java.sql.Timestamp"});

        // Wrappers Map
        MAP_WRAPPERS.put(boolean.class, Boolean.class);
        MAP_WRAPPERS.put(byte.class, Byte.class);
        MAP_WRAPPERS.put(char.class, Character.class);
        MAP_WRAPPERS.put(double.class, Double.class);
        MAP_WRAPPERS.put(float.class, Float.class);
        MAP_WRAPPERS.put(int.class, Integer.class);
        MAP_WRAPPERS.put(long.class, Long.class);
        MAP_WRAPPERS.put(short.class, Short.class);
        MAP_WRAPPERS.put(void.class, Void.class);
        MAP_WRAPPERS.put(Boolean.class, boolean.class);
        MAP_WRAPPERS.put(Byte.class, byte.class);
        MAP_WRAPPERS.put(Character.class, char.class);
        MAP_WRAPPERS.put(Double.class, double.class);
        MAP_WRAPPERS.put(Float.class, float.class);
        MAP_WRAPPERS.put(Integer.class, int.class);
        MAP_WRAPPERS.put(Long.class, long.class);
        MAP_WRAPPERS.put(Short.class, short.class);
        MAP_WRAPPERS.put(Void.class, void.class);

        // Default primitive values
        MAP_DEFAULT_VALUES.put(boolean.class, false);
        MAP_DEFAULT_VALUES.put(byte.class, 0);
        MAP_DEFAULT_VALUES.put(char.class, ' ');
        MAP_DEFAULT_VALUES.put(double.class, 0.0);
        MAP_DEFAULT_VALUES.put(float.class, 0.0f);
        MAP_DEFAULT_VALUES.put(int.class, 0);
        MAP_DEFAULT_VALUES.put(long.class, 0L);
        MAP_DEFAULT_VALUES.put(short.class, 0);
        MAP_DEFAULT_VALUES.put(Boolean.class, false);
        MAP_DEFAULT_VALUES.put(Byte.class, 0);
        MAP_DEFAULT_VALUES.put(Character.class, ' ');
        MAP_DEFAULT_VALUES.put(Double.class, 0.0);
        MAP_DEFAULT_VALUES.put(Float.class, 0.0f);
        MAP_DEFAULT_VALUES.put(Integer.class, 0);
        MAP_DEFAULT_VALUES.put(Long.class, 0L);
        MAP_DEFAULT_VALUES.put(Short.class, 0);
        MAP_DEFAULT_VALUES.put(String.class, "");

        try {
            Class rsClass = ResultSet.class;
            MAP_TYPES.put("Object", rsClass.getMethod("getObject", String.class));
            MAP_TYPES.put("boolean", rsClass.getMethod("getBoolean", String.class));
            MAP_TYPES.put("Boolean", rsClass.getMethod("getBoolean", String.class));
            MAP_TYPES.put("byte", rsClass.getMethod("getByte", String.class));
            MAP_TYPES.put("Byte", rsClass.getMethod("getByte", String.class));
            MAP_TYPES.put("short", rsClass.getMethod("getShort", String.class));
            MAP_TYPES.put("Short", rsClass.getMethod("getShort", String.class));
            MAP_TYPES.put("int", rsClass.getMethod("getInt", String.class));
            MAP_TYPES.put("Integer", rsClass.getMethod("getInt", String.class));
            MAP_TYPES.put("long", rsClass.getMethod("getLong", String.class));
            MAP_TYPES.put("Long", rsClass.getMethod("getLong", String.class));
            MAP_TYPES.put("float", rsClass.getMethod("getFloat", String.class));
            MAP_TYPES.put("Float", rsClass.getMethod("getFloat", String.class));
            MAP_TYPES.put("double", rsClass.getMethod("getDouble", String.class));
            MAP_TYPES.put("Double", rsClass.getMethod("getDouble", String.class));
            MAP_TYPES.put("byte[]", rsClass.getMethod("getBytes", String.class));
            MAP_TYPES.put("Array", rsClass.getMethod("getArray", String.class));
            MAP_TYPES.put("URL", rsClass.getMethod("getURL", String.class));
            MAP_TYPES.put("Ref", rsClass.getMethod("getRef", String.class));
            MAP_TYPES.put("String", rsClass.getMethod("getString", String.class));
            MAP_TYPES.put("Time", rsClass.getMethod("getTime", String.class));
            MAP_TYPES.put("Date", rsClass.getMethod("getDate", String.class));
            MAP_TYPES.put("BigDecimal", rsClass.getMethod("getBigDecimal", String.class));
            MAP_TYPES.put("Timestamp", rsClass.getMethod("getTimestamp", String.class));
            MAP_TYPES.put("InputStream", rsClass.getMethod("getBinaryStream", String.class));
            MAP_TYPES.put("ByteArrayInputStream", rsClass.getMethod("getBinaryStream",
                    String.class));
            MAP_TYPES.put("Reader", rsClass.getMethod("getCharacterStream", String.class));
            MAP_TYPES.put("InputStreamReader", rsClass.getMethod("getCharacterStream",
                    String.class));
            MAP_TYPES.put("Blob", rsClass.getMethod("getBlob", String.class));
            MAP_TYPES.put("Clob", rsClass.getMethod("getClob", String.class));
            MAP_TYPES.put("NClob", rsClass.getMethod("getNClob", String.class));
            MAP_TYPES.put("SQLXML", rsClass.getMethod("getSQLXML", String.class));
        } catch (NoSuchMethodException ex) {
            StackTracer.printStackTrace(ex);
        } catch (SecurityException ex) {
            StackTracer.printStackTrace(ex);
        }
    }

    /**
     * Gets the wrapper datatype name for the input primitive type. (like
     * Integer for int, Float for float etc)
     *
     * @param primitive The name of the primitive datatype (int, float etc...)
     * @return The wrapper class name for the given primitive type
     */
    public static String getWrapperNameForPrimitive(String primitive) {
        if (primitive.equalsIgnoreCase("int")) {
            return "Integer";
        } else if (primitive.equalsIgnoreCase("char")) {
            return "Character";
        } else if (primitive.startsWith("Big")) {
            return primitive;
        }

        String header = primitive.substring(0, 1).toUpperCase();
        String body = primitive.substring(1).toLowerCase();

        return header + body;
    }

    /**
     * Gets the {@code JdbcResultSetMetaData} for the given {@code ResultSet}.
     * {@code JdbcResultSetMetaData} contains some necessary parameters of a
     * result set like the column count, row count, and column columnNames
     * etc.<br>Note: This method does not close the resultSet. The caller of
     * this method is responsible for closing the ResultSet. This method also
     * calls the {@code beforeFirst()} method of the {@code ResultSet}.
     *
     * @param resultSet The resultSet object from which the meta data is to be
     * extracted
     * @return the description of this ResultSet object's columns
     * @throws java.sql.SQLException if a database access error occurs or this
     * method is called on a closed result set
     * @see JdbcResultSetMetaData
     * @see ResultSet#beforeFirst()
     *
     */
    public static final JdbcResultSetMetaData getMetaDataForResultSet(ResultSet resultSet)
            throws SQLException {
        int cursorPosition = resultSet.getRow();
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();
        Map<String, JdbcResultSetMetaDataColumn> columns = new LinkedHashMap();
        for (int i = 1; i <= columnCount; i++) {

            String tableName = metaData.getTableName(i);
            String columnName = metaData.getColumnName(i);
            String columnLabel = metaData.getColumnLabel(i);
            String columnTitle = String.format("%s.%s", tableName, columnName);

            int columnType = metaData.getColumnType(i);
            String columnClassName = metaData.getColumnClassName(i);

//            if (metaData.getTableAlias(i).isEmpty()) {
//                columnTitle = metaData.getColumnLabel(i);
//            }
            JdbcResultSetMetaDataColumn column = new JdbcResultSetMetaDataColumn(i, columnName,
                    columnLabel, columnTitle, columnType, columnClassName, tableName);

//            System.out.println(column);
            
            columns.put(columnTitle, column);
        }

        resultSet.last();
        int rowCount = resultSet.getRow();
        if (cursorPosition == 0) {
            resultSet.beforeFirst();
        } else {
            resultSet.absolute(cursorPosition);
        }
        return new JdbcResultSetMetaData(columnCount, rowCount, columns);
    }

    /**
     * Gets a CSV-format String from the given ResultSet. <br>Note: This method
     * does not close the resultSet. The caller of this method is responsible
     * for closing the ResultSet.
     *
     * @param resultSet The ResultSet object from which CSV formatted String is
     * to be returned
     * @param separator The column or field separator. Usually a semi-colon (;)
     * or comma (,) is used as a separator
     * @param includeColumnNames A flag indicating whether column names should
     * be included in the output String or not
     * @return The data from ResultSet in a CSV-format
     * @throws SQLException If the ResultSet is already closed or null
     */
    public static final String toCsvString(ResultSet resultSet, char separator,
            boolean includeColumnNames)
            throws SQLException {
        if (resultSet == null || resultSet.isClosed()) {
            throw new SQLException();
        }
        JdbcResultSetMetaData metaData = JdbcUtils.getMetaDataForResultSet(resultSet);
        int _columnCount = metaData.getColumnCount();
        String _columnNames;
        String lineSeperator = System.getProperty("line.separator");
        StringBuilder sb = new StringBuilder();
        if (includeColumnNames) {
            Set<String> cols = metaData.getColumns().keySet();
            _columnNames = cols.toString().replaceAll("\\[", "").replaceAll("\\]", "")
                    .replaceAll(" ", "");
            sb = new StringBuilder(_columnNames);
            sb.append(lineSeperator);
        }
        while (resultSet.next()) {
            for (int i = 1; i <= _columnCount; i++) {
                sb.append(resultSet.getString(i));
                if (i != _columnCount) {
                    sb.append(separator);
                }
            }
            sb.append(lineSeperator);
        }
        return sb.toString();
    }

    /**
     * Saves the ResultSet to a file in a CSV format. <br>Note: This method does
     * not close the resultSet. The caller of this method is responsible for
     * closing the ResultSet.
     *
     * @param resultSet The ResultSet object from which CSV formatted String is
     * to be returned
     * @param separator The column or field separator. Usually a semi-colon (;)
     * or comma (,) is used as a separator
     * @param includeColumnNames A flag indicating whether column names should
     * be included in the output String or not
     * @param filePath The destination path (including the file name and file
     * type) where file has to written
     * @return The newly created CSV File
     * @throws SQLException If the ResultSet is already closed or null
     * @throws IOException If an I/O error occurs
     * @see #toCsvString(java.sql.ResultSet, char, boolean)
     */
    public static final File toCsvFile(ResultSet resultSet, char separator,
            boolean includeColumnNames, String filePath)
            throws SQLException, IOException {
        String csvString = JdbcUtils.toCsvString(resultSet, separator, includeColumnNames);
        File file = new File(filePath);
        // write to file
        Writer writer = new FileWriter(file);
        try {
            writer.write(csvString);
        } finally {
            writer.flush();
            writer.close();
        }
        return file;
    }

    /**
     * Saves the ResultSet to a file in a CSV format. This overloaded function
     * takes the file object instead of the filePath.<br>Note: This method does
     * not close the resultSet. The caller of this method is responsible for
     * closing the ResultSet.
     *
     * @param resultSet The ResultSet object from which CSV formatted String is
     * to be returned
     * @param separator The column or field separator. Usually a semi-colon (;)
     * or comma (,) is used as a separator
     * @param includeColumnNames A flag indicating whether column names should
     * be included in the output String or not
     * @param file The file in which CSV String has to be written
     * @return The CSV File
     * @throws SQLException If the ResultSet is already closed or null
     * @throws IOException If an I/O error occurs
     * @see #toCsvString(java.sql.ResultSet, char, boolean)
     */
    public static final File toCsvFile(ResultSet resultSet, char separator,
            boolean includeColumnNames, File file)
            throws SQLException, IOException {
        String csvString = JdbcUtils.toCsvString(resultSet, separator, includeColumnNames);
        // write to file
        Writer writer = new FileWriter(file);
        try {
            writer.write(csvString);
        } finally {
            writer.flush();
            writer.close();
        }
        return file;
    }

    /**
     * Converts the given string into the camel case.
     * <p>
     * &nbsp;</p> Like hello_world to HelloWorld or helloWorld (depending upon
     * the value of firstLetterCapital flag)
     *
     * @param input The input String
     * @param firstLetterCapital flag to determine if the first letter of output
     * string be Capital or Small
     * @return The string in camel case.
     */
    public static String toCamelCase(String input, boolean firstLetterCapital) {
        Blob b;
        StringBuilder sb = new StringBuilder();
        for (String s : input.split("_")) {
            sb.append(Character.toUpperCase(s.charAt(0)));
            if (s.length() > 1) {
                sb.append(s.substring(1, s.length()).toLowerCase());
            }
        }

        if (!firstLetterCapital) {
            String firstLetter = sb.toString().substring(0, 1).toLowerCase();
            sb.replace(0, 1, firstLetter);
        }

        return sb.toString();
    }
}
