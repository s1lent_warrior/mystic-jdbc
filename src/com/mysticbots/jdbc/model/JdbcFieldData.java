/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.model;

import java.lang.reflect.Field;

/**
 *
 * @author s1lent_warrior
 */
public class JdbcFieldData {

    private final Field field;
    private final Object fieldValue;
    private final JdbcResultSetMetaDataColumn column;

    public JdbcFieldData(Field field, Object fieldValue, JdbcResultSetMetaDataColumn column) {
        this.field = field;
        this.fieldValue = fieldValue;
        this.column = column;
    }

    public Field getField() {
        return field;
    }

    public Object getFieldValue() {
        return fieldValue;
    }

    public JdbcResultSetMetaDataColumn getColumn() {
        return column;
    }

    @Override
    public String toString() {
        return "JdbcFieldData { "
                + "fieldName = '" + field.getName() + "', "
                + "fieldType = '" + field.getType().getName() + "', "
                + "fieldValue = '" + fieldValue + "', "
                + "JdbcColumn = '" + column.getTableName() + "." + column.getColumnName()
                + "' }";
    }
}
