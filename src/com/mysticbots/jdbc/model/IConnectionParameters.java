/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.model;

/**
 * The Connection Parameters interface. It contains method signatures that
 * contain necessary parameters to establish a connection to a database.<br>
 * Additionally this interface can be implemented to connect to any other
 * database.<br>This method is also implemented by
 * <code>JdbcConnectionParameters</code> class.
 *
 * @author S1LENT W@RRIOR
 * @version 1.0
 */
public interface IConnectionParameters {

    public enum DatabaseType {
        MYSQL("com.mysql.jdbc.Driver");

        private DatabaseType(String driver) {
            this.driverName = driver;
        }

        public final String driverName;
    }

    /**
     * Gives the Database Type. Implement this method to return your database
     * type.<br>Supported types are declared in
     * <code>com.mysticbots.jdbc.model.IConnectionParameters.DatabaseType</code>
     * <br>for example: In case of MySQL, it this method should return:
     * <code>com.mysticbots.jdbc.model.IConnectionParameters.DatabaseType.MYSQL</code>
     *
     * @return The Database type
     */
    public DatabaseType getDatabaseType();

    /**
     * Gives the Database Driver name. Implement this method to return your
     * database driver's name.<br>for example: In case of MySQL, it this method
     * should return: <code>com.mysql.jdbc.Driver</code>
     *
     * @return The Database driver name
     */
    public String getDriverName();

    /**
     * Gives the Connection String using the
     * {@link #getDatabaseAddress()}, {@link #getDatabasePort()}, {@link #getDatabaseName()}
     * <br>Implement this method to return your database Connection String.
     * <br>For example: In case of MySQL server deployed at
     * <code>localhost</code> at port <code>3306</code> for the database named
     * <code>"test_jdbc"</code>, this method should return:
     * <code>jdbc:mysql://localhost:3306/test_jdbc</code>.
     *
     * @return The Connection String
     *
     * @see #getConnectionString()
     * @see #getDatabasePort()
     * @see #getDatabaseName()
     */
    public String getConnectionString();

    /**
     * Gives the Connection Address. Implement this method to return your
     * database Address.<br>for example: In case of MySQL server deployed at
     * localhost, this method should either return <code>localhost</code> or
     * <code>127.0.0.1</code><br>.
     *
     * @return The Connection Address
     */
    public String getDatabaseAddress();

    /**
     * Gives the Connection Port. Implement this method to return your database
     * Connection Port.<br>for example: In case of MySQL server deployed at
     * localhost at port 3306, this method should return: <code>3306/</code>.
     *
     * @return The Connection Port
     */
    public int getDatabasePort();

    /**
     * Gives the name of the database, to which you want to connect on your
     * database server.
     *
     * @return The name of the database
     */
    public String getDatabaseName();

    /**
     * Gives the username used to authenticate the user on Database
     *
     * @return The username
     */
    public String getUsername();

    /**
     * Gives the password used to authenticate the user on Database. In case of
     * no password, implement this method to return an empty String or null
     *
     * @return The password
     */
    public String getPassword();
}
