/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.model;

import java.util.HashMap;
import java.util.Map;

/**
 * The <code>IConnectionParameters</code> implementation for MySQL JDBC.
 * Override its abstract methods to return your own parameters.<br>Side note: In
 * order to avoid re-typing the same code, it is advised to keep a static object
 * of this class in its child class like this:
 * <pre><code>
 * public class MySqlJdbcConnectionParameters extends JdbcConnectionParameters {
 *
 *     public static JdbcConnectionParameters params = new MySqlJdbcConnectionParameters();
 *
 *     // overridden methods goes here
 * }
 * </code>
 * Then in the calling class:
 * <code>
 * public class Test extends JdbcUtility {
 *
 *      public Test() {
 *          super(MySqlJdbcConnectionParameters.params);
 *      }
 *
 *      // other methods and logic comes here
 * }
 * </code></pre><br>For more details, see the sample code
 *
 * @author S1LENT W@RRIOR
 * @version 1.0
 */
public abstract class JdbcConnectionParameters implements IConnectionParameters {

    private static final String FORMAT_HOST = "<HOST>";
    private static final String FORMAT_PORT = "<PORT>";
    private static final String FORMAT_DB_NAME = "<DATABASE_NAME>";

    private static final Map<DatabaseType, String> DATABASE_URLS = new HashMap() {
        private static final long serialVersionUID = 3109256773218160485L;

        {
            this.put(DatabaseType.MYSQL, "jdbc:mysql://<HOST>:<PORT>/<DATABASE_NAME>");
        }
    };

    /**
     * Gives the Database Driver name.
     *
     * @return The JDBC Driver Name
     */
    @Override
    public String getDriverName() {
        return this.getDatabaseType().driverName;
    }

    /**
     * Gives the Connection String using the
     * {@link #getDatabaseAddress()}, {@link #getDatabasePort()}, {@link #getDatabaseName()}
     * <br>For example: In case of MySQL server deployed at
     * <code>localhost</code> at port <code>3306</code> for the database named
     * <code>"test_jdbc"</code>, this method should return:
     * <code>jdbc:mysql://localhost:3306/test_jdbc</code>
     *
     * @return The Connection String
     *
     * @see #getConnectionString()
     * @see #getDatabasePort()
     * @see #getDatabaseName()
     */
    @Override
    public String getConnectionString() {
        String format = DATABASE_URLS.get(this.getDatabaseType());

        String formatted = format.replace(FORMAT_HOST, this.getDatabaseAddress())
                .replace(FORMAT_PORT, Integer.toString(this.getDatabasePort()))
                .replace(FORMAT_DB_NAME, this.getDatabaseName());

        return formatted;
    }

    /**
     * This abstract method needs to be overridden to return your database
     * address
     *
     * @return The database address
     */
    @Override
    public abstract DatabaseType getDatabaseType();

    /**
     * This abstract method needs to be overridden to return your database
     * address
     *
     * @return The database address
     */
    @Override
    public abstract String getDatabaseAddress();

    /**
     * This abstract method needs to be overridden to return your database port
     *
     * @return The database port
     */
    @Override
    public abstract int getDatabasePort();

    /**
     * This abstract method needs to be overridden to return your database name
     *
     * @return The database name
     */
    @Override
    public abstract String getDatabaseName();

    /**
     * This abstract method needs to be overridden to return your MySQL username
     *
     * @return The user name
     */
    @Override
    public abstract String getUsername();

    /**
     * This abstract method needs to be overridden to give your MySQL password
     *
     * @return The password
     */
    @Override
    public abstract String getPassword();
}
