/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.model;

/**
 * An object that holds information about some basic information properties of
 * the designated column in a {@code ResultSet} object.
 *
 * @see JdbcResultSetMetaData
 * @author s1lent_warrior
 */
public class JdbcResultSetMetaDataColumn {

    private final int columnIndex;
    private final String columnName;
    private final String columnAlias;
    private final String columnTitle;
    private final int columnType;
    private final String columnClassName;
    private final String tableName;

    public JdbcResultSetMetaDataColumn(int columnIndex, String columnName, String columnLabel,
            String columnTitle, int columnType, String columnTypeName, String tableName) {
        this.columnIndex = columnIndex;
        this.columnName = columnName;
        this.columnAlias = columnLabel;
        this.columnTitle = columnTitle;
        this.columnType = columnType;
        this.columnClassName = columnTypeName;
        this.tableName = tableName;
    }

    /**
     * Gets the column number of the designated column in a {@code ResultSet}.
     * <br>The column Index starts from 1,2,3....
     *
     * @return the column index
     */
    public int getColumnIndex() {
        return columnIndex;
    }

    /**
     * Retrieves the name of the designated column in a {@code ResultSet}.
     *
     * @return the column name
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * Gets the alias of the designated column defined in {@code AS} clause in a
     * {@code ResultSet}.<br>If no alias is defined, the column name is
     * returned.
     *
     * @return the column alias
     * @see #getColumnName()
     */
    public String getColumnAlias() {
        return columnAlias;
    }

    /**
     * Gets the column title (i.e: in {@code table_name.column_name} format) of
     * the designated column in a {@code ResultSet}
     *
     * @return column title in {@code table_name.column_name} format
     */
    public String getColumnTitle() {
        return columnTitle;
    }

    /**
     * Retrieves the designated column's SQL type.
     *
     * @return SQL type from java.sql.Types
     * @see java.sql.Types
     */
    public int getColumnType() {
        return columnType;
    }

    /**
     * Returns the fully-qualified name of the Java class for the designated
     * column.
     *
     * @return the fully-qualified name of the class in the Java programming
     * language
     */
    public String getColumnClassName() {
        return columnClassName;
    }

    /**
     * Gets the designated column's table name or alias.
     *
     * @return table name / alias or "" if not applicable
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * Gets the designated column's table alias defined in the {@code AS} clause
     * of a query for the designated column. If no table alias is defined, this
     * method returns the table name for the designated column.
     *
     * @return table alias or table name if no table alias defined
     * @see #getTableName()
     *
     */
//    public String getTableAlias() {
//        return tableAlias;
//    }
    @Override
    public String toString() {
        return "JdbcColumn{"
                + "columnIndex=" + columnIndex + ", "
                + "columnName=" + columnName + ", "
                + "columnLabel=" + columnAlias + ", "
                + "columnTitle=" + columnTitle + ", "
                + "columnType=" + columnType + ", "
                + "columnTypeName=" + columnClassName + ", "
                + "tableName=" + tableName
                + '}';
    }
}
