/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc.model;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * An object that can be used to get information about some basic properties of
 * the columns in a {@code ResultSet} object.
 *
 * @see com.mysticbots.jdbc.JdbcUtils#getMetaDataForResultSet(ResultSet)
 *
 * @author s1lent_warrior
 */
public class JdbcResultSetMetaData {

    private final int columnCount;
    private final int rowCount;
    private final List<String> tables;
    private final Map<String, JdbcResultSetMetaDataColumn> columns;

    public JdbcResultSetMetaData(int columnCount, int rowCount,
            Map<String, JdbcResultSetMetaDataColumn> columns) {
        this.columnCount = columnCount;
        this.rowCount = rowCount;
        this.columns = columns;

        Set<String> unsortedTables = new TreeSet();
        for (Map.Entry<String, JdbcResultSetMetaDataColumn> entry : columns.entrySet()) {
            JdbcResultSetMetaDataColumn column = entry.getValue();
            unsortedTables.add(column.getTableName().toLowerCase());
        }
        this.tables = new ArrayList(unsortedTables);
    }

    /**
     * Retrieves number of columns in a {@code ResultSet}.
     *
     * @return the column count
     */
    public int getColumnCount() {
        return columnCount;
    }

    /**
     * Gets the number of rows in a {@code ResultSet}
     *
     * @return the row count
     */
    public int getRowCount() {
        return rowCount;
    }

    /**
     * Retrieves a {@link  java.util.List} of table names queried in a
     * {@code ResultSet}
     *
     * @return list of table names
     */
    public List<String> getTables() {
        return tables;
    }

    /**
     * Gets a {@link  java.util.Map} with {@code key=full column name} (i.e: in
     * {@code table_name.column_name} format) and
     * {@code value = JdbcResultSetMetaDataColumn} object.
     *
     * @return a {@code java.util.Map} of {@link JdbcResultSetMetaDataColumn}
     * @see JdbcResultSetMetaDataColumn
     */
    public Map<String, JdbcResultSetMetaDataColumn> getColumns() {
        return columns;
    }

    /**
     * Finds the {@link JdbcResultSetMetaDataColumn} for the given column title.
     *
     * @param columnTitle The column title. <br>Note: {@code columnTitle} should
     * be in {@code table_name.column_name} format NOT
     * {@code table_alias.column_name}, OR {@code table_name.column_alias}, OR
     * {@code table_alias.column_alias}
     * @return the {@code JdbcResultSetMetaDataColumn} or {@code null} if no
     * column found
     */
    @SuppressWarnings(value = "Don't remove commented code before further testing")
    public JdbcResultSetMetaDataColumn findColumnFromTitle(String columnTitle) {
        String[] split = columnTitle.split("\\.");
        for (Map.Entry<String, JdbcResultSetMetaDataColumn> entry : columns.entrySet()) {
            JdbcResultSetMetaDataColumn column = entry.getValue();
            if (split.length == 2) {
                if (this.tables.contains(split[0].toLowerCase())) {
                    String colTitle = column.getTableName() + "." + column.getColumnName();
                    if (columnTitle.equals(colTitle)) {
                        return column;
                    }
                }
            }
// don't remove before furhter testing
//            if (split.length == 2) {
//                if (this.tables.contains(split[0].toLowerCase())) {
//                    String _columnTitle = column.getTableName() + "." + column.getColumnAlias();
//                    if (column.getTableName().equals(split[0])) {
////                        System.out.println(columnTitle + ": " + _columnTitle);
//                    }
//                    System.out.println(column.getTableName().equals(split[0]));
////                    System.out.println(columnTitle + ": " + _columnTitle);
//                    if (columnTitle.equals(_columnTitle)) {
//                        return column;
//                    } else if (split[0].equals(column.getTableName())
//                            && split[1].equals(column.getColumnAlias())) {
//                        return column;
//                    }
//                } else {
//                    return null;
//                }
//            } else if (split[1].equals(column.getColumnTitle())) {
//                return column;
//            } else {
//                return null;
//            }
        }
        return null;
    }
}
