/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc;

import com.mysticbots.jdbc.annotations.JdbcAttribute;
import com.mysticbots.jdbc.annotations.JdbcAttributeCollection;
import com.mysticbots.jdbc.annotations.JdbcAttributeObject;
import com.mysticbots.jdbc.annotations.JdbcEntity;
import com.mysticbots.jdbc.exceptions.AnnotationNotFoundException;
import com.mysticbots.jdbc.model.JdbcFieldData;
import com.mysticbots.jdbc.model.JdbcResultSetMetaData;
import com.mysticbots.jdbc.model.JdbcResultSetMetaDataColumn;
import com.mysticbots.util.ObjectConverter;
import com.mysticbots.util.StackTracer;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is Core of the Database class which do the following tasks: <br> 1.
 * Establishes connection with the Database<br> 2. Perform Transactions between
 * the Database and the Layers above this class<br> 3. Disconnect from database
 * and release the resources<br>This class contains all methods needed to query
 * the database. It can be used to perform any DML (insert, update, delete )
 * operation.
 *
 * @author Sarfraz Ahmad
 * @author S1LENT W@RRIOR
 * @version 1.0
 */
public final class MysticJdbc {

    private static final int KEY_ATTRIBUTES = 1;
    private static final int KEY_ATTRIBUTE_OBJECTS = 2;
    private static final int KEY_ATTRIBUTE_COLLECTIONs = 3;

    private static int UNIQUE_ID = 0;
    private final int uniqueId = ++UNIQUE_ID;

    @Override
    public int hashCode() {
        return uniqueId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MysticJdbc other = (MysticJdbc) obj;
        return this.uniqueId == other.uniqueId;
    }

    private final Connection connection; // Connection object to establish the connectin to the database

    private static final Map<Integer, List<Statement>> statements = new ConcurrentHashMap();
    private static final Map<Integer, List<ResultSet>> resultSets = new ConcurrentHashMap();

    public final MysticQueryBuilder queryBuilder;

    /**
     * The only constructor of this class. It Attempts to establish a connection
     * with the Database
     *
     * @param params the connection parameters
     */
    public MysticJdbc(Connection connection) {
        this.connection = connection;
        this.queryBuilder = new MysticQueryBuilder(this);
    }

    /**
     * Gets the {@link java.sql.Connection}
     *
     * @return The connection object
     */
    public Connection getConnection() {
        return this.connection;
    }

    private List<Statement> getStatements() {
        if (!statements.containsKey(this.uniqueId)) {
            statements.put(uniqueId, new ArrayList());
        }

        return statements.get(this.uniqueId);
    }

    private List<ResultSet> getResultSets() {
        if (!resultSets.containsKey(this.uniqueId)) {
            resultSets.put(uniqueId, new ArrayList());
        }

        return resultSets.get(this.uniqueId);
    }

    /**
     * Returns a PreparedStatement of the given connection, set with the given
     * SQL query and the given parameter values.<br>Note: This method must be
     * used for SELECT and UPDATE queries. To Prepare an INSERT query, use
     * {@link MysticJdbc#prepareInsertStatement(String, Object[])}
     * method<br>Side Note: You can use this method for INSERT query as well but
     * you won't be able to get AUTO GENERATED keys in return!
     *
     * @param query The SQL query to construct the PreparedStatement with.
     * @param values The parameter values to be set in the created
     * PreparedStatement.
     * @return The Prepared Statement object
     * @throws SQLException If something fails while creating the
     * PreparedStatement.
     */
    public PreparedStatement prepareStatement(String query, Object... values) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query,
                ResultSet.TYPE_SCROLL_SENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        this.setValues(preparedStatement, values);
        this.getStatements().add(preparedStatement);
        return preparedStatement;
    }

    /**
     * Returns a PreparedStatement of the given connection, set with the given
     * SQL query and the given parameter values.<br>Note: This method is
     * suggested to be used for INSERT queries only. To Prepare a SELECT or
     * UPDATE query, use {@link MysticJdbc#prepareStatement(String, Object[])}
     * method
     * <br>Side Note: You can use this method for SELECT or UPDATE queries as
     * well but you won't get an UPDATEABLE ResultSet
     *
     * @param query The SQL query to construct the PreparedStatement with.
     * @param values The parameter values to be set in the created
     * PreparedStatement.
     * @return The Prepared Statement object
     * @throws SQLException If something fails while creating the
     * PreparedStatement.
     */
    public PreparedStatement prepareInsertStatement(String query, Object... values)
            throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS);
        this.setValues(preparedStatement, values);
        this.getStatements().add(preparedStatement);
        return preparedStatement;
    }

    /**
     * Set the given parameter values in the given
     * {@link java.sql.PreparedStatement}.
     *
     * @param preparedStatement the prepared statement
     * @param values The parameter values to be set in the created
     * PreparedStatement.
     * @throws SQLException If something fails while setting the
     * PreparedStatement values.
     */
    private void setValues(PreparedStatement preparedStatement, Object... values)
            throws SQLException {
        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                preparedStatement.setObject(i + 1, values[i]);
            }
        }
    }

    /**
     * Inserts row(s) into the database and returns its Auto Generated Key (if
     * applicable)
     * <p>
     * &nbsp;</p>Note: You can INSERT rows using
     * {@code executeUpdate(String query, Object... values)} method. But this
     * this method is useful if you need the Auto-Generated Key in return
     *
     * @param <T> The type of object(s) annotated with
     * {@link com.mysticbots.jdbc.annotations.JdbcEntity} annotation
     * @param objects The object(s) to be inserted in the database
     * @return The Auto Generated Key
     * @throws SQLException if INSERT fails
     * @throws NullPointerException If no objects are found
     * @throws IllegalAccessException If reflection exception occurs
     * @throws IllegalArgumentException If reflection exception occurs
     * @throws AnnotationNotFoundException if <code> T
     * </code> is not annotated with
     * {@link com.mysticbots.jdbc.annotations.JdbcEntity} annotation
     */
//    public <T extends Object> Integer insertObjects(T... objects)
//            throws AnnotationNotFoundException, NullPointerException, IllegalAccessException,
//            IllegalArgumentException, SQLException {
//
//        return this.insertObjects(new String[]{}, false, objects);
//    }
    /**
     * Inserts row(s) into the database and returns its Auto Generated Key (if
     * applicable)
     * <p>
     * &nbsp;</p>Note: You can INSERT rows using
     * {@code executeUpdate(String query, Object... values)} method. But this
     * this method is useful if you need the Auto-Generated Key in return
     *
     * @param <T> The type of object(s) annotated with
     * {@link com.mysticbots.jdbc.annotations.JdbcEntity} annotation
     * @param ignoreNulls Decides whether to insert null properties to database
     * or not
     * @param objects The object(s) to be inserted in the database
     * @return The Auto Generated Key
     * @throws SQLException if INSERT fails
     * @throws NullPointerException If no objects are found
     * @throws IllegalAccessException If reflection exception occurs
     * @throws IllegalArgumentException If reflection exception occurs
     * @throws AnnotationNotFoundException if <code> T
     * </code> is not annotated with
     * {@link com.mysticbots.jdbc.annotations.JdbcEntity} annotation
     */
    public <T extends Object> Integer insertObjects(boolean ignoreNulls, T... objects)
            throws AnnotationNotFoundException, NullPointerException, IllegalAccessException,
            IllegalArgumentException, SQLException {

        return this.insertObjects(new String[]{}, ignoreNulls, objects);
    }

    /**
     * Inserts row(s) into the database and returns its Auto Generated Key (if
     * applicable)
     * <p>
     * &nbsp;</p>Note: You can INSERT rows using
     * {@code executeUpdate(String query, Object... values)} method. But this
     * this method is useful if you need the Auto-Generated Key in return
     *
     * @param <T> The type of object(s) annotated with
     * {@link com.mysticbots.jdbc.annotations.JdbcEntity} annotation
     * @param exclude The comma-separated string of columns to be excluded from
     * insert
     * @param ignoreNulls Decides whether to insert null properties to database
     * or not
     * @param objects The object(s) to be inserted in the database
     * @return The Auto Generated Key
     * @throws SQLException if INSERT fails
     * @throws NullPointerException If no objects are found
     * @throws IllegalAccessException If reflection exception occurs
     * @throws IllegalArgumentException If reflection exception occurs
     * @throws AnnotationNotFoundException if <code> T
     * </code> is not annotated with
     * {@link com.mysticbots.jdbc.annotations.JdbcEntity} annotation
     */
    public <T extends Object> Integer insertObjects(String exclude, boolean ignoreNulls,
            T... objects) throws AnnotationNotFoundException, NullPointerException,
            IllegalAccessException, IllegalArgumentException, SQLException {

        String[] excludeArr = MysticQueryBuilder.getArrayFromString(exclude);

        return this.insertObjects(excludeArr, ignoreNulls, objects);
    }

    /**
     * Inserts row(s) into the database and returns its Auto Generated Key (if
     * applicable)
     * <p>
     * &nbsp;</p>Note: You can INSERT rows using
     * {@code executeUpdate(String query, Object... values)} method. But this
     * this method is useful if you need the Auto-Generated Key in return
     *
     * @param <T> The type of object(s) annotated with
     * {@link com.mysticbots.jdbc.annotations.JdbcEntity} annotation
     * @param exclude The array column names to be excluded from insert
     * @param ignoreNulls Decides whether to insert null properties to database
     * or not
     * @param objects The object(s) to be inserted in the database
     * @return The Auto Generated Key
     * @throws SQLException if INSERT fails
     * @throws NullPointerException If no objects are found
     * @throws IllegalAccessException If reflection exception occurs
     * @throws IllegalArgumentException If reflection exception occurs
     * @throws AnnotationNotFoundException if <code> T
     * </code> is not annotated with
     * {@link com.mysticbots.jdbc.annotations.JdbcEntity} annotation
     */
    public <T extends Object> Integer insertObjects(String[] exclude, boolean ignoreNulls,
            T... objects) throws AnnotationNotFoundException, NullPointerException,
            IllegalAccessException, IllegalArgumentException, SQLException {

        Class clazz = objects[0].getClass();
        MysticQueryBuilder.validateClasses(clazz);

        String[] excludedColumns = this.getExcludedColumns(clazz, exclude, ignoreNulls, objects);

        String query = this.queryBuilder.getInsertQuery(clazz, objects.length, excludedColumns);
        Object[] values = this.queryBuilder.getObjectsForInsert(excludedColumns, objects).toArray();
        return this.executeInsert(query, values);
    }

    private <T extends Object> String[] getExcludedColumns(Class clazz, String[] exclude,
            boolean ignoreNulls, T... objects) throws IllegalArgumentException,
            IllegalAccessException {
        if (ignoreNulls == true) {
            List<String> excludedColumns = new ArrayList(Arrays.asList(exclude));
            for (T object : objects) {
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    JdbcAttributeObject entityObject = field.getAnnotation(JdbcAttributeObject.class);
                    if (entityObject == null) { // plain object
                        JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
                        if (attribute != null && !excludedColumns.contains(attribute.columnName())) {
                            Object value = field.get(object);
                            if (value == null && !attribute.defaultValue().isEmpty()) {
                                value = attribute.defaultValue();
                            }

                            if (value == null) {
                                excludedColumns.add(attribute.columnName());
                            }
                        }
                    }
                }
            }

            return excludedColumns.toArray(new String[0]);
        }

        return exclude;
    }

    /**
     * Inserts row(s) into the database and returns its Auto Generated Key (if
     * applicable). For multiple insertions, this method will return the key of
     * last inserted row.<p>
     * </p>Note: You can INSERT rows using
     * {@code executeUpdate(String query, Object... values)} method. But this
     * this method is useful if you need the Auto-Generated Key in return
     *
     * @param query The SQL string having an INSERT query
     * @param values The input value(s)
     * @return The Last Auto Generated Key
     * @throws SQLException if INSERT fails
     */
    public Integer executeInsert(String query, Object... values) throws SQLException {
        PreparedStatement preparedStatement = this.prepareInsertStatement(query, values);
        preparedStatement.executeUpdate();
        ResultSet rs = preparedStatement.getGeneratedKeys();
        this.getResultSets().add(rs);
        while (rs.next()) {
            if (rs.isLast()) {
                return rs.getInt(1);
            }
        }
        return 0;
    }

    /**
     * Executes any DML (INSERT, UPDATE or DELETE) query on the Database and
     * return the number of rows affected.<br>May also return 0 for a query
     * returning no result.
     *
     * @param query The SQL string having an UPDATE query
     * @param values The input value(s)
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     * statements or (2) 0 for SQL statements that return nothing
     * @throws SQLException if UPDATE fails
     */
    public Integer executeUpdate(String query, Object... values) throws SQLException {
        PreparedStatement preparedStatement = this.prepareStatement(query, values);
        return preparedStatement.executeUpdate();
    }

    /**
     * Retrieves Records from the database. Can take any number of input
     * parameters as search criterion
     *
     * @param query the SQL string having a SELECT query
     * @param values the input key(s) or the search criteria
     * @return the ResultSet of the given query
     * @throws SQLException if a database access error occurs; this method is
     * called on a closed PreparedStatement or the SQL statement does not return
     * a ResultSet object
     */
    public ResultSet selectFromDatabase(String query, Object... values) throws SQLException {
        ResultSet resultSet;
        PreparedStatement preparedStatement = this.prepareStatement(query, values);
        resultSet = preparedStatement.executeQuery();
        this.getResultSets().add(resultSet);
        return resultSet;
    }

    /**
     * Gets an object of type {@code T}, annotated with {@link JdbcEntity} from
     * the given query.
     *
     * @param <T> The generic type object to be returned
     * @param query The query to be executed
     * @param values The input key or the search criteria
     * @param clazz The class of the generic object to be returned
     * @return The objects {@code Map} of the given type {@code T} or
     * {@code null} if the {@code ResultSet} is either closed or the cursor is
     * after the last row.
     * @throws com.mysticbots.jdbc.exceptions.AnnotationNotFoundException
     * @throws java.sql.SQLException
     * @throws Exception in following cases:<br>
     * <pre>
     * 1. If {@code clazz} is null.
     * 2. If a database access error occurs.
     * 3. If this method is called on a closed PreparedStatement.
     * 4. If the SQL statement does not return a ResultSet object.
     * 5. All exceptions thrown by {@link java.lang.reflect.Method#invoke(Object, Object...)}
     * 6. All exceptions thrown by {@link java.lang.Class#newInstance()}
     * 7. All exceptions thrown by {@link java.lang.reflect.Field#set(Object, Object)}
     * </pre>
     */
    public <T> T getJdbcEntityFromQuery(String query, Class<T> clazz, Object... values)
            throws AnnotationNotFoundException, SQLException, Exception {
        if (clazz == null) {
            throw new NullPointerException("Class can not be null!");
        }

        if (!clazz.isAnnotationPresent(JdbcEntity.class)) {
            throw new AnnotationNotFoundException("JdbcEntity annotation not found on "
                    + clazz.getName());
        }

        ResultSet rs = this.selectFromDatabase(query, values);
        T object = this.getObjectFromResultSet(rs, clazz);

        return object;
    }

    /**
     * Gets a {@link java.util.List} of objects of type {@code T}, annotated
     * with {@link JdbcEntity} from the given query.
     *
     * @param <T> The generic type object to be returned
     * @param query The query to be executed
     * @param values The input key or the search criteria
     * @param clazz The class of the generic object to be returned
     * @return The objects {@code List} of the given type {@code T} or
     * {@code null} if the {@code ResultSet} is either closed or the cursor is
     * after the last row.
     * @throws Exception in following cases:<br>
     * <pre>
     * 1. If {@code clazz} is null.
     * 2. If a database access error occurs.
     * 3. If this method is called on a closed PreparedStatement.
     * 4. If the SQL statement does not return a ResultSet object.
     * 5. All exceptions thrown by {@link java.lang.reflect.Method#invoke(Object, Object...)}
     * 6. All exceptions thrown by {@link java.lang.Class#newInstance()}
     * 7. All exceptions thrown by {@link java.lang.reflect.Field#set(Object, Object)}
     * </pre>
     */
    public <T> List<T> getObjectsAsListFromQuery(String query, Class<T> clazz, Object... values)
            throws Exception {
        if (clazz == null) {
            throw new NullPointerException("Class can not be null!");
        }
        ResultSet rs = this.selectFromDatabase(query, values);
        List<T> objects = this.getObjectsAsListFromResultSet(rs, clazz);

        return objects;
    }

    /**
     * Gets a {@link java.util.Map} of objects of type {@code T}, annotated with
     * {@link JdbcEntity} from the given query.
     *
     * @param <K> The type of the key for the Map
     * @param <V> The generic type object to be returned
     * @param query The query to be executed
     * @param values The input key or the search criteria
     * @param clazz The class of the generic object to be returned
     * @return The objects {@code Map} of the given type {@code T} or
     * {@code null} if the {@code ResultSet} is either closed or the cursor is
     * after the last row.
     * @throws Exception in following cases:<br>
     * <pre>
     * 1. If {@code clazz} is null.
     * 2. If a database access error occurs.
     * 3. If this method is called on a closed PreparedStatement.
     * 4. If the SQL statement does not return a ResultSet object.
     * 5. All exceptions thrown by {@link java.lang.reflect.Method#invoke(Object, Object...)}
     * 6. All exceptions thrown by {@link java.lang.Class#newInstance()}
     * 7. All exceptions thrown by {@link java.lang.reflect.Field#set(Object, Object)}
     * </pre>
     */
    public <K extends Object, V> Map<K, V> getObjectsAsMapFromQuery(String query, Class<V> clazz,
            Object... values) throws Exception {
        if (clazz == null) {
            throw new NullPointerException("Class can not be null!");
        }
        ResultSet rs = this.selectFromDatabase(query, values);
        Map<K, V> objects = this.getObjectsAsMapFromResultSet(rs, clazz);

        return objects;
    }

    /**
     * Gets an object of type {@code T}, annotated with {@link JdbcEntity} from
     * the given {@code ResultSet} at the specified row number. The row number
     * must not be zero negative.<br>Note: This method does not close the
     * {@code ResultSet}. The caller of this method is responsible for closing
     * the {@code ResultSet}.
     *
     * @param <T> The generic type object to be returned
     * @param resultSet The {@code ResultSet} object from which the generic
     * object is to be extracted
     * @param clazz The class of the generic object to be returned
     * @param row The row number starting from 1,2,3,....
     * @return The object of the given type {@code T} or {@code null} if the
     * {@code ResultSet} is either closed or the row number is zero or negative
     * @throws Exception in following cases:<br>
     * <pre>
     * 1. If{@code clazz} is{@code null}
     * 2. If{@code row <= 0}
     * 3. If the given{@code ResultSet} is{@code null}
     * 4. All exceptions thrown by {@link java.lang.reflect.Method#invoke(Object, Object...)}
     * 5. All exceptions thrown by {@link java.lang.Class#newInstance()}
     * 6. All exceptions thrown by {@link java.lang.reflect.Field#set(Object, Object)}
     * </pre>
     */
    public <T> T getObjectFromResultSet(ResultSet resultSet, Class<T> clazz, int row)
            throws Exception {
        if (clazz == null) {
            throw new NullPointerException("Class can not be null!");
        }
        int currentCounter = resultSet.getRow();
        if (row <= 0) {
            throw new IllegalArgumentException("Class can not be null!");
        } else if (row == 1) {
            resultSet.beforeFirst();
        } else {
            resultSet.absolute(row - 1);
        }
        T object = this.getObjectFromResultSet(resultSet, clazz);
        resultSet.absolute(currentCounter);
        return object;
    }

    /**
     * Gets an object of type {@code T}, annotated with {@link JdbcEntity} from
     * the given {@code ResultSet}.<br>Note: This method does not close the
     * {@code ResultSet}. The caller of this method is responsible for closing
     * the {@code ResultSet}.
     *
     * @param <T> The generic type object to be returned
     * @param resultSet The {@code ResultSet} object from which the generic
     * object is to be extracted
     * @param clazz The class of the generic object to be returned
     * @return The object of the given type {@code T} or {@code null} if the
     * {@code ResultSet} is either closed or the cursor is after the last row.
     * @throws java.sql.SQLException
     * @throws Exception in following cases:<br>
     * <pre>
     * 1. If{@code clazz} is{@code null}
     * 2. If the given{@code ResultSet} is{@code null}
     * 3. All exceptions thrown by {@link java.lang.reflect.Method#invoke(Object, Object...)}
     * 4. All exceptions thrown by {@link java.lang.Class#newInstance()}
     * 5. All exceptions thrown by {@link java.lang.reflect.Field#set(Object, Object)}
     * </pre>
     */
    public <T> T getObjectFromResultSet(ResultSet resultSet, Class<T> clazz)
            throws SQLException, Exception {
        Map<Object, T> objects = this.getObjectsAsMapFromResultSet(resultSet, clazz);
        return (T) ((objects != null && objects.size() > 0)
                ? new ArrayList(objects.values()).get(0) : null);
    }

    /**
     * Gets a {@link java.util.List} of objects of type {@code T}, annotated
     * with {@link JdbcEntity} from the given {@code ResultSet}.<br>Note: This
     * method does not close the {@code ResultSet}. The caller of this method is
     * responsible for closing the {@code ResultSet}.
     *
     * @param <T> The generic type object to be returned
     * @param resultSet The {@code ResultSet} object from which the generic
     * object is to be extracted
     * @param clazz The class of the generic object to be returned
     * @return The objects {@code List} of the given type {@code T} or
     * {@code null} if the {@code ResultSet} is either closed or the cursor is
     * after the last row.
     * @throws Exception in following cases:<br>
     * <pre>
     * 1. If {@code clazz} is null.
     * 2. If the given {@code ResultSet} is  null
     * 3. All exceptions thrown by {@link java.lang.reflect.Method#invoke(Object, Object...)}
     * 4. All exceptions thrown by {@link java.lang.Class#newInstance()}
     * 5. All exceptions thrown by {@link java.lang.reflect.Field#set(Object, Object)}
     * </pre>
     */
    public <T> List<T> getObjectsAsListFromResultSet(ResultSet resultSet, Class<T> clazz)
            throws AnnotationNotFoundException, SQLException, Exception {
        Map<Object, T> objects = this.getObjectsAsMapFromResultSet(resultSet, clazz);
        return (objects != null) ? new ArrayList(objects.values()) : new ArrayList();
    }

    /**
     * Gets a {@link java.util.Map} of objects of type {@code T}, annotated with
     * {@link JdbcEntity} from the given {@code ResultSet}.<br>Note: This method
     * does not close the {@code ResultSet}. The caller of this method is
     * responsible for closing the {@code ResultSet}.
     *
     * @param <K> The type of the key for the Map
     * @param <V> The generic type object to be returned
     * @param resultSet The {@code ResultSet} object from which the generic
     * object is to be extracted
     * @param clazz The class of the generic object to be returned
     * @return The objects {@code Map} of the given type {@code T} or
     * {@code null} if the {@code ResultSet} is either closed or the cursor is
     * after the last row.
     * @throws java.sql.SQLException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.InstantiationException
     * @throws Exception in following cases:<br>
     * <pre>
     * 1. If {@code clazz} is null.
     * 2. If the given {@code ResultSet} is  null
     * 3. All exceptions thrown by {@link java.lang.reflect.Method#invoke(Object, Object...)}
     * 4. All exceptions thrown by {@link java.lang.Class#newInstance()}
     * 5. All exceptions thrown by {@link java.lang.reflect.Field#set(Object, Object)}
     * </pre>
     *
     * @throws com.mysticbots.jdbc.exceptions.AnnotationNotFoundException
     */
    public <K extends Object, V> Map<K, V> getObjectsAsMapFromResultSet(ResultSet resultSet,
            Class<V> clazz) throws SQLException, IllegalAccessException, InstantiationException,
            AnnotationNotFoundException, Exception {

        // get meta data from resultset
        JdbcResultSetMetaData metaData = JdbcUtils.getMetaDataForResultSet(resultSet);

        // get all rows map from resultset here, so that it may not be needed again and again
        List<Map<String, JdbcFieldData>> rows = this.getRowsMapFromResultSet(resultSet,
                metaData, clazz);

//        System.err.println("Rows: " + rows);
        // Get map from the rows data
        // this method recursively calls itself
        Map<K, V> allRootObjects = this.getMap(clazz, rows, metaData);

        return allRootObjects;
    }

    public List<Map<String, Object>> getRowsFromResultSet(ResultSet resultSet) {

        if (resultSet == null) {
            return new ArrayList();
        }

        List<Map<String, Object>> rows = new ArrayList();

        try {
            // get meta data from resultset
            JdbcResultSetMetaData metaData = JdbcUtils.getMetaDataForResultSet(resultSet);

            resultSet.beforeFirst();
            while (resultSet.next()) {
                Map<String, Object> rowMap = new LinkedHashMap();
                for (JdbcResultSetMetaDataColumn column : metaData.getColumns().values()) {
                    Object value = resultSet.getObject(column.getColumnName());
                    String columnTitle = column.getTableName() + "." + column.getColumnName();
                    rowMap.put(columnTitle, value);
                }
                rows.add(rowMap);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MysticJdbc.class.getName()).log(Level.SEVERE, null, ex);
        }

        return rows;
    }

    public <T> T getObjectFromRow(Map<String, Object> row, Class<T> clazz)
            throws InstantiationException, IllegalAccessException, AnnotationNotFoundException {

        // get JdbcEntity Annotation from class
        JdbcEntity entity = clazz.getAnnotation(JdbcEntity.class);
        if (entity == null) {
            throw new AnnotationNotFoundException("JdbcEntity annotation not found on "
                    + clazz.getName());
        }

        T object = clazz.newInstance();

        for (Field field : clazz.getDeclaredFields()) {

            JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
            if (attribute != null) {
                field.setAccessible(true);

                String columnName = entity.tableName() + "." + attribute.columnName();

                Object fieldValue = row.get(columnName);
                if (fieldValue == null) {
                    fieldValue = attribute.defaultValue();
                }
//                System.err.println(field.getName() + " = " + fieldValue);
//                field.set(object, fieldValue);
                try {
                    Class fieldType = field.getType();
                    field.set(object, ObjectConverter.convert(fieldValue, fieldType));
                } catch (Exception ex) {
                    System.err.println(ex.getClass() + ": " + field.getName() + " = " + fieldValue);
                    System.err.println("From: " + fieldValue.getClass() + "\t To: " + field.getType());
                }
            }

        }

        return object;
    }

    /**
     * Gets a {@link java.util.Map} of objects of type {@code T}, annotated with
     * {@link JdbcEntity} from the given {@code ResultSet}.<br>Note: This method
     * does not close the {@code ResultSet}. The caller of this method is
     * responsible for closing the {@code ResultSet}.
     *
     * @param <K> The type of the key for the Map
     * @param <V> The generic type object to be returned
     * @param resultSet The {@code ResultSet} object from which the generic
     * object is to be extracted
     * @param clazz The class of the generic object to be returned
     * @return The objects {@code Map} of the given type {@code T} or
     * {@code null} if the {@code ResultSet} is either closed or the cursor is
     * after the last row.
     * @throws java.sql.SQLException
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.InstantiationException
     * @throws Exception in following cases:<br>
     * <pre>
     * 1. If {@code clazz} is null.
     * 2. If the given {@code ResultSet} is  null
     * 3. All exceptions thrown by {@link java.lang.reflect.Method#invoke(Object, Object...)}
     * 4. All exceptions thrown by {@link java.lang.Class#newInstance()}
     * 5. All exceptions thrown by {@link java.lang.reflect.Field#set(Object, Object)}
     * </pre>
     *
     * @throws com.mysticbots.jdbc.exceptions.AnnotationNotFoundException
     */
    public <K extends Object, V> Map<K, V> getObjectsAsMapFromRows(List<Map<String, Object>> rows,
            Class<V> clazz) throws SQLException, IllegalAccessException, InstantiationException,
            AnnotationNotFoundException, Exception {

        // get JdbcEntity Annotation from class
        JdbcEntity entity = clazz.getAnnotation(JdbcEntity.class);
        if (entity == null) {
            throw new AnnotationNotFoundException("JdbcEntity annotation not found on "
                    + clazz.getName());
        }

        Field rootKeyColumn = this.getPrimaryKeyColumn(clazz);
        if (rootKeyColumn == null) {
            throw new Exception("No Primary key column defined!");
        }

        // the map of object to be returned
        Map<K, V> objects = new LinkedHashMap();

        for (Map<String, Object> row : rows) {
            V object = this.getObjectFromRow(row, clazz);
            objects.put((K) rootKeyColumn.get(object), object);
        }

        return objects;
    }

    /**
     * Gets map from the rows data. This method recursively calls itself to set
     * values in inner collections
     *
     * @param <K> The Key of map
     * @param <V> The Object annotated with {@link JdbcAttributeCollection}
     * @param clazz The class of the object
     * @param rows The data retrieved from all rows of a resultset
     * @param metaData The {@link JdbcResultSetMetaData}
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws AnnotationNotFoundException
     * @throws Exception
     */
    private <K, V> Map<K, V> getMap(Class<V> clazz, List<Map<String, JdbcFieldData>> rows,
            JdbcResultSetMetaData metaData) throws IllegalArgumentException, IllegalAccessException,
            InstantiationException, AnnotationNotFoundException, Exception {

        // get JdbcEntity Annotation from class
        JdbcEntity rootEntity = clazz.getAnnotation(JdbcEntity.class);
        if (rootEntity == null) {
            throw new AnnotationNotFoundException("JdbcEntity annotation not found on "
                    + clazz.getName());
        }

        Field rootKeyColumn = this.getPrimaryKeyColumn(clazz);
        if (rootKeyColumn == null) {
            throw new Exception("No Primary key column defined!");
        }

        // create a Map containing all key columns of all entities
        Map<String, Field> keyColumnsMap = new HashMap();
        keyColumnsMap.put(rootEntity.tableName(), rootKeyColumn);

        // create a map containing all JdbcEntities
        Map<Class, JdbcEntity> entitiesMap = new HashMap();
        entitiesMap.put(clazz, rootEntity);

        Map<Field, Class> attributeObjects = this.getFieldsWithAnnotationFromEntity(clazz,
                JdbcAttributeObject.class);

        // iterate over the attributeObjects and populate data inside keyColumnsMap & entitiesMap
        this.populateEntitiesAndColumns(attributeObjects, keyColumnsMap, entitiesMap);

        Map<Field, Class> attributeCollections = this.getFieldsWithAnnotationFromEntity(clazz,
                JdbcAttributeCollection.class);

        // iterate over the attributeCollections and populate data inside keyColumnsMap & entitiesMap
        this.populateEntitiesAndColumns(attributeCollections, keyColumnsMap, entitiesMap);

        // list of json-style row data, key = fieldName, value = fieldValue
        List<Map<String, Object>> allRowsData = new ArrayList();

        // the map of object to be returned
        Map<K, V> allRootObjects = new LinkedHashMap();

//        System.err.println("Rows: " + rows);
        // iterating over each row
        for (Map<String, JdbcFieldData> row : rows) {

            JdbcEntity entity = entitiesMap.get(clazz);
            Field keyColumn = keyColumnsMap.get(entity.tableName());

            // get each object from a row
            V object = this.getNewObject(row, metaData, clazz, null);

            // save it into the map to be returned
            allRootObjects.put((K) keyColumn.get(object), object);

            allRowsData.add(this.getMapForRowData(row, metaData, clazz));

//            System.err.println(object);
        }

//        System.err.println(attributeObjects.values());
//        check why attribute objects are not setting
        for (V rootObject : allRootObjects.values()) {
            // check and set collections in JdbcAttributeObjects
            this.setAttributesInObject(attributeObjects, rows, metaData, allRowsData,
                    rootObject, rootKeyColumn);

            // check and set collections in JdbcAttributeCollections
            this.setAttributesInObject(attributeCollections, rows, metaData, allRowsData,
                    rootObject, rootKeyColumn);
        }

        return allRootObjects;
    }

    /**
     *
     * @param attributesMap
     * @param keyColumnsMap
     * @param entitiesMap
     * @throws SecurityException
     */
    private void populateEntitiesAndColumns(Map<Field, Class> attributesMap,
            Map<String, Field> keyColumnsMap, Map<Class, JdbcEntity> entitiesMap)
            throws SecurityException {
        // iterate over the attributesMap, (either attributeObjects or attributeCollections,
        // and populate data inside keyColumnsMap & entitiesMap
        for (Class keyClass : attributesMap.values()) {
            JdbcEntity objectEntity = (JdbcEntity) keyClass.getAnnotation(JdbcEntity.class);
            Field objectKey = this.getPrimaryKeyColumn(keyClass);
            if (objectEntity != null && objectKey != null) {
                objectKey.setAccessible(true);
                keyColumnsMap.put(objectEntity.tableName(), objectKey);
                entitiesMap.put(keyClass, objectEntity);
            }
        }
    }

    /**
     * Sets collections
     *
     * @param <T>
     * @param attributesMap
     * @param rows
     * @param metaData
     * @param allRowsData
     * @param rootObject
     * @param rootKeyColumn
     * @throws Exception
     */
    private <T> void setAttributesInObject(Map<Field, Class> attributesMap,
            List<Map<String, JdbcFieldData>> rows, JdbcResultSetMetaData metaData,
            List<Map<String, Object>> allRowsData, T rootObject, Field rootKeyColumn)
            throws Exception {
        int counter = 0;
        for (Map.Entry<Field, Class> entry : attributesMap.entrySet()) {
            Field foreignField = entry.getKey();
            Class foreignClass = entry.getValue();

            foreignField.setAccessible(true);
            Map foreignObjectsMap;// = this.getMap(foreignClass, rows, metaData);
            if (this.hasAttributeCollectionAnnotation(foreignClass)) {  //  Attribute Collection
                foreignObjectsMap = this.getMap(foreignClass, rows, metaData);
            } else {    // Attribute Object
                List<Map<String, JdbcFieldData>> currentRow = new ArrayList();
                currentRow.add(rows.get(counter++));

                foreignObjectsMap = this.getMap(foreignClass, currentRow, metaData);
//                foreignObjectsMap = this.getMapForObject(allRowsData, rootObject,
//                        rootKeyColumn, foreignField, foreignClass);
                // fix this!
                // System.err.println("F: "+allRowsData);
            }

            Object fieldValue = null;
            if (foreignField.getType().isArray()) {
                List list = new ArrayList(foreignObjectsMap.values());
                fieldValue = Array.newInstance(foreignClass, foreignObjectsMap.size());
                int length = Array.getLength(fieldValue);
                for (int i = 0; i < length; i++) {
                    Array.set(fieldValue, i, list.get(i));
                }
            } else if (Collection.class.isAssignableFrom(foreignField.getType())) {
                if (List.class.isAssignableFrom(foreignField.getType())) {
                    List list = new ArrayList(foreignObjectsMap.values());
                    fieldValue = list;
                } else if (Set.class.isAssignableFrom(foreignField.getType())) {
                    Set set = new LinkedHashSet(foreignObjectsMap.values());
                    fieldValue = set;
                }
            } else if (Map.class.isAssignableFrom(foreignField.getType())) {
                fieldValue = foreignObjectsMap;
            } else if (foreignObjectsMap != null && !foreignObjectsMap.isEmpty()) {
                fieldValue = foreignObjectsMap.values().toArray()[0];
            }

            foreignField.set(rootObject, fieldValue);
        }
    }

    /**
     *
     * @param <T>
     * @param clazz
     * @return
     */
    private <T> Map<Field, Class> getFieldsWithAnnotationFromEntity(Class<T> clazz,
            Class<? extends Annotation> annotationClass) {
        Map<Field, Class> classes = new HashMap();

        for (Field field : clazz.getDeclaredFields()) {
            Annotation annotation = field.getAnnotation(annotationClass);
            if (annotation != null) {
                if (annotationClass.equals(JdbcAttributeObject.class)) {
                    classes.put(field, field.getType());
                } else if (annotationClass.equals(JdbcAttributeCollection.class)) {
                    classes.put(field, ((JdbcAttributeCollection) annotation).tableClass());
                }
            }
        }

        return classes;
    }

    /**
     *
     * @param clazz
     * @return
     */
    private boolean hasAttributeCollectionAnnotation(Class clazz) {
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(JdbcAttributeCollection.class)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param <T>
     * @param allObjects
     * @param object
     * @param keyField
     * @param foreignField
     * @param foreignFieldClass
     * @return
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private <T> Map getMapForObject(List<Map<String, Object>> allObjects, T object,
            Field keyField, Field foreignField, Class foreignFieldClass)
            throws IllegalArgumentException, IllegalAccessException, InstantiationException {
        Map objects = new LinkedHashMap();
        Field foreignKeyField = this.getPrimaryKeyColumn(foreignFieldClass);

        Object rootKeyValue = keyField.get(object);
        for (int i = 0; i < allObjects.size(); i++) {
            Map<String, Object> anObjectMap = allObjects.get(i);
            if (rootKeyValue.equals(anObjectMap.get(keyField.getName()))) {
                if (foreignField.isAnnotationPresent(JdbcAttributeCollection.class)) {
                    Map<String, Object> foreignObjectMap
                            = (Map) anObjectMap.get(foreignField.getName());
                    if (foreignObjectMap != null) {
                        JdbcAttributeCollection attribute
                                = foreignField.getAnnotation(JdbcAttributeCollection.class);
                        Object listObject = this.getObjectFromMap(foreignObjectMap,
                                attribute.tableClass(), null);

                        // remove any object with 0 or null key
                        Object defaultValue
                                = JdbcUtils.MAP_DEFAULT_VALUES.get(foreignKeyField.getType());
                        Object keyValue = foreignKeyField.get(listObject);
                        if (keyValue != null && !keyValue.equals(defaultValue)) {
                            objects.put(foreignKeyField.get(listObject), listObject);
                        }
                    }
                }
            }
        }

        return objects;
    }

    /**
     *
     * @param <T>
     * @param map
     * @param clazz
     * @param newInstance
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private <T> T getObjectFromMap(Map<String, Object> map, Class<T> clazz, T newInstance)
            throws InstantiationException, IllegalAccessException {
        JdbcEntity entity = (JdbcEntity) clazz.getAnnotation(JdbcEntity.class);
        if (newInstance == null) {
            newInstance = clazz.newInstance();
        }

        Class superClass = entity.parentEntityClass();
        if (superClass != null && superClass.getAnnotation(JdbcEntity.class) != null) {
            this.getObjectFromMap(map, superClass, newInstance);
        }

        Stack<Class> classesStack = new Stack();
        classesStack.push(clazz);

        while (!classesStack.isEmpty()) {
            Class aClass = classesStack.pop();
            for (Field field : aClass.getDeclaredFields()) {
                field.setAccessible(true);

                if (field.isAnnotationPresent(JdbcAttribute.class)) { // plain object
                    Object fieldValue = map.get(field.getName());
                    Object defaultValue = JdbcUtils.MAP_DEFAULT_VALUES.get(field.getType());
                    if (fieldValue != null && !fieldValue.equals(defaultValue)) {
                        field.set(newInstance, fieldValue);
                    }

                } else if (field.isAnnotationPresent(JdbcAttributeObject.class)) { // JdbcAttributeObject
                    classesStack.push(field.getType());
                }
            }
        }
        return newInstance;
    }

    /**
     *
     * @param <T>
     * @param row
     * @param metaData
     * @param clazz
     * @return
     * @throws Exception
     */
    private <T> Map<String, Object> getMapForRowData(Map<String, JdbcFieldData> row,
            JdbcResultSetMetaData metaData, Class<T> clazz) throws Exception {
        Map<String, Object> map = new HashMap();

        JdbcEntity entity = (JdbcEntity) clazz.getAnnotation(JdbcEntity.class);

        Class superClass = entity.parentEntityClass();
        if (superClass != null && superClass.getAnnotation(JdbcEntity.class) != null) {
            map.putAll(this.getMapForRowData(row, metaData, superClass));
        }

        Stack<Class> classesStack = new Stack();
        classesStack.push(clazz);

        while (!classesStack.isEmpty()) {
            Class aClass = classesStack.pop();

            for (Field field : aClass.getDeclaredFields()) {
                field.setAccessible(true);

                if (field.isAnnotationPresent(JdbcAttribute.class)) { // plain object
                    JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
                    if (attribute != null) {
                        String title = entity.tableName() + "." + attribute.columnName();
                        if (!attribute.tableName().isEmpty()) {
                            title = attribute.tableName() + "." + attribute.columnName();
                        }
                        JdbcResultSetMetaDataColumn column = metaData.findColumnFromTitle(title);
                        if (column != null) {
                            JdbcFieldData fieldData = row.get(column.getColumnTitle());
                            Object fieldValue = fieldData.getFieldValue();
                            Object defaultValue = JdbcUtils.MAP_DEFAULT_VALUES.get(field.getType());
                            if (fieldValue != null && !fieldValue.equals(defaultValue)) {
                                map.put(field.getName(), fieldValue);
                            }
                        }
                    }
                } else if (field.isAnnotationPresent(JdbcAttributeObject.class)) { // JdbcAttributeObject
                    JdbcAttributeObject attribute = field.getAnnotation(JdbcAttributeObject.class);
                    boolean hasTable = false;
                    for (String table : metaData.getTables()) {
                        if (table.equalsIgnoreCase(attribute.tableName())) {
                            hasTable = true;
                            break;
                        }
                    }
                    if (hasTable) {
                        classesStack.push(field.getType());
                    }
                } else if (field.isAnnotationPresent(JdbcAttributeCollection.class)) {
                    // JdbcAttributeCollection
                    JdbcAttributeCollection attribute
                            = field.getAnnotation(JdbcAttributeCollection.class);
                    boolean hasTable = false;
                    for (String table : metaData.getTables()) {
                        if (table.equalsIgnoreCase(attribute.tableName())) {
                            hasTable = true;
                            break;
                        }
                    }
                    if (hasTable) {
                        classesStack.push(attribute.tableClass());
                    }
                }
            }
        }

        return map;
    }

    /**
     *
     * @param <T>
     * @param clazz
     * @return
     */
    private <T> Field getPrimaryKeyColumn(Class<T> clazz) {
        JdbcEntity entity = clazz.getAnnotation(JdbcEntity.class);
        Class parentClass = entity.parentEntityClass();

        if (parentClass.equals(JdbcEntity.class)) {
            for (Field field : clazz.getDeclaredFields()) {
                JdbcAttribute annotation = field.getAnnotation(JdbcAttribute.class);
                if (annotation != null) {
                    if (annotation.isKeyColumn()) {
                        field.setAccessible(true);
                        return field;
                    }
                }
            }

            return null;
        } else {
            return this.getPrimaryKeyColumn(parentClass);
        }
    }

    /**
     *
     * @param rs
     * @param metaData
     * @param clazz
     * @return
     * @throws Exception
     */
    private List<Map<String, JdbcFieldData>> getRowsMapFromResultSet(ResultSet rs,
            JdbcResultSetMetaData metaData, Class clazz) throws Exception {
        List<Map<String, JdbcFieldData>> rows = new ArrayList();
        Map<String, Field> fieldsMap = this.getFieldsMapForClass(clazz, metaData);
        while (rs.next()) {
            Map<String, JdbcFieldData> innerObjects = new LinkedHashMap();
            Set<Map.Entry<String, JdbcResultSetMetaDataColumn>> entrySet
                    = metaData.getColumns().entrySet();
            for (Map.Entry<String, JdbcResultSetMetaDataColumn> entry : entrySet) {
                String columnTitle = entry.getKey();
                Field field = fieldsMap.get(columnTitle);
//                System.err.println(columnTitle + ": " + field);
                if (field != null) {
                    field.setAccessible(true);
                    Method method = JdbcUtils.MAP_TYPES.get(field.getType().getSimpleName());
//                    System.out.println(field.getName() + " COL VAL: " + method.getName());
                    JdbcFieldData fieldData = null;
                    try {
//                        rs.geto
                        JdbcResultSetMetaDataColumn column = entry.getValue();
                        Object columnValue = method.invoke(rs, column.getColumnName());
//                        Object columnValue = rs.getObject(column.getColumnIndex(), field.getType());
                        fieldData = new JdbcFieldData(field, columnValue, entry.getValue());
                    } catch (Exception ex) {
                        if (ex.getCause() instanceof SQLException) {
//                            System.err.println("Error: " + ex.getMessage());
                        } else {
                            System.err.println("Error: " + ex.getMessage());
                            throw ex;
                        }
                    }

//                    System.out.println("Data: " + fieldData);
                    innerObjects.put(columnTitle, fieldData);
                }
            }
            rows.add(innerObjects);
        }
        return rows;
    }

    /**
     *
     * @param clazz
     * @param metaData
     * @return
     */
    private Map<String, Field> getFieldsMapForClass(Class clazz, JdbcResultSetMetaData metaData) {
        Map<String, Field> fieldsMap = new LinkedHashMap();

        Stack<Class> classesStack = new Stack();
        classesStack.push(clazz);
        while (!classesStack.isEmpty()) {

            Class aClass = classesStack.pop();
            JdbcEntity entity = (JdbcEntity) aClass.getAnnotation(JdbcEntity.class);
            Class parentClass = entity.parentEntityClass();
            if (!parentClass.equals(JdbcEntity.class)) {
                Map<String, Field> fields = this.getFieldsMapForClass(parentClass, metaData);
                fieldsMap.putAll(fields);
            }

            for (Field field : aClass.getDeclaredFields()) {
                field.setAccessible(true);

                if (field.isAnnotationPresent(JdbcAttribute.class)) { // plain object
                    JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
                    String fieldTitle = entity.tableName() + "." + attribute.columnName();
                    if (!attribute.tableName().isEmpty()) {
                        fieldTitle = attribute.tableName() + "." + attribute.columnName();
                    }

                    JdbcResultSetMetaDataColumn column = metaData.findColumnFromTitle(fieldTitle);

                    if (column != null) {
                        fieldsMap.put(column.getColumnTitle(), field);
                    }
                } else if (field.isAnnotationPresent(JdbcAttributeObject.class)) {
                    classesStack.push(field.getType());
                } else if (field.isAnnotationPresent(JdbcAttributeCollection.class)) {
                    JdbcAttributeCollection attribute
                            = field.getAnnotation(JdbcAttributeCollection.class);
                    classesStack.push(attribute.tableClass());
                }
            }
        }

        return fieldsMap;
    }

    /**
     *
     * @param <T>
     * @param row
     * @param metaData
     * @param clazz
     * @param newInstance
     * @return
     * @throws Exception
     */
    private <T> T getNewObject(Map<String, JdbcFieldData> row, JdbcResultSetMetaData metaData,
            Class<T> clazz, T newInstance) throws Exception {
        JdbcEntity entity = (JdbcEntity) clazz.getAnnotation(JdbcEntity.class);
        if (newInstance == null) {
            newInstance = clazz.newInstance();
        }

        Class superClass = entity.parentEntityClass();
        if (superClass != null && superClass.getAnnotation(JdbcEntity.class) != null) {
            this.getNewObject(row, metaData, superClass, newInstance);
        }

        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);

            if (field.isAnnotationPresent(JdbcAttribute.class)) { // plain object
                JdbcAttribute attribute = field.getAnnotation(JdbcAttribute.class);
                if (attribute != null) {
                    String title = entity.tableName() + "." + attribute.columnName();
                    if (!attribute.tableName().isEmpty()) {
                        title = attribute.tableName() + "." + attribute.columnName();
                    }
                    JdbcResultSetMetaDataColumn column = metaData.findColumnFromTitle(title);
                    if (column != null) {
                        JdbcFieldData fieldData = row.get(column.getColumnTitle());
                        Object fieldValue = fieldData.getFieldValue();
                        if (fieldValue == null) {
                            fieldValue = attribute.defaultValue();
                        }

                        if (fieldValue != null) {
//                            System.out.println("---------------------------------------");
//                            System.out.println("VALUE: " + fieldValue);
//                            System.out.println("VALUE TYPE: " + fieldValue.getClass());
//                            System.out.println("FIELD TYPE: " + field.getType());
//                            System.out.println("---------------------------------------\n\n");

                            Class valueType = fieldValue.getClass();
                            Class fieldType = field.getType();
                            if (valueType != fieldType) {
                                fieldValue = ObjectConverter.convert(fieldValue, fieldType);
                            }

                            field.set(newInstance, fieldValue);
                        }
                    }
                }
            }
        }

        return newInstance;
    }

    /**
     * This method retrieves a single object from the Database.
     *
     * @param <T> The generic type of object to be retrieved
     * @param query the SQL string having a SELECT query
     * @param clazz The class of object to be retrieved
     * @param defaultValue The default value to be returned for empty ResultSet
     * @param values the input key or the Search criteria
     * @return A single integer value
     * @throws SQLException if a database access error occurs; this method is
     * called on a closed PreparedStatement or the SQL statement does not return
     * a ResultSet object
     */
    public <T> T getObjectFromQuery(String query, Class<T> clazz, T defaultValue, Object... values)
            throws Exception {
        List<T> objects = this.getObjectsFromQuery(query, clazz, values);
        if (objects != null && !objects.isEmpty()) {
            return objects.get(0);
        }

        if (defaultValue == null && JdbcUtils.MAP_WRAPPERS.containsKey(clazz)) {
            return (T) JdbcUtils.MAP_DEFAULT_VALUES.get(clazz);
        }

        return defaultValue;
    }

    /**
     * This method is same as {@link #getObjectFromQuery(java.lang.String,
     * java.lang.Class, java.lang.Object, java.lang.Object...)
     * }. The only difference is that it returns a List of objects
     *
     * @param <T> The generic type of object to be retrieved
     * @param query the SQL string having a SELECT query
     * @param clazz The class of object to be retrieved
     * @param values the input key or the search criteria
     * @return List of integers
     * @throws SQLException if a database access error occurs; this method is
     * called on a closed PreparedStatement or the SQL statement does not return
     * a ResultSet object
     */
    public <T> List<T> getObjectsFromQuery(String query, Class<T> clazz, Object... values)
            throws SQLException {
        List objects = new ArrayList();
        ResultSet resultSet = this.selectFromDatabase(query, values);
        while (resultSet.next()) {
            try {
                objects.add(resultSet.getObject(1, clazz));
            } catch (SQLException ex) {
                objects.add((T) resultSet.getObject(1));
            }
        }

        return objects;
    }

    /**
     * Returns complete <code>ResultSet</code> in the form of
     * <code>List<List<Object>></code>
     *
     * @param query the SQL string having a SELECT query
     * @param values the input key or the search criteria
     * @return List of integers
     * @throws SQLException if a database access error occurs; this method is
     * called on a closed PreparedStatement or the SQL statement does not return
     * a ResultSet object
     */
    public List<List<Object>> getObjectsCollectionFromQuery(String query, Object... values)
            throws SQLException {
        List<List<Object>> objects = new ArrayList();
        ResultSet resultSet = this.selectFromDatabase(query, values);
        JdbcResultSetMetaData metaData = JdbcUtils.getMetaDataForResultSet(resultSet);
        while (resultSet.next()) {
            List<Object> row = new ArrayList();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                row.add(resultSet.getObject(i));
            }

            objects.add(row);
        }

        return objects;
    }

//    // TODO: javadoc
//    /**
//     * This method is same as {@link #getObjectFromQuery(java.lang.String,
//     * java.lang.Class, java.lang.Object, java.lang.Object...)
//     * }. The only difference is that it returns a List of objects
//     *
//     * @param <T> The generic type of object to be retrieved
//     * @param query the SQL string having a SELECT query
//     * @param clazz The class of object to be retrieved
//     * @param values the input key or the search criteria
//     * @return List of integers
//     * @throws SQLException if a database access error occurs; this method is
//     * called on a closed PreparedStatement or the SQL statement does not return
//     * a ResultSet object
//     */
//    public List<Map<String, Object>> getObjectsCollectionMapFromQuery(String query, Object... values)
//            throws SQLException {
//        this.openConnection();
//        List<Map<String, Object>> objects = new ArrayList();
//        ResultSet resultSet = this.selectFromDatabase(query, values);
//        JdbcResultSetMetaData metaData = JdbcUtils.getMetaDataForResultSet(resultSet);
//        while (resultSet.next()) {
//            Map<String, Object> row = new HashMap();
//            for (String columnKeys = ) {
//                row.put(metaData.get, values)
//            }
//        }
//
//        return objects;
//    }
    /**
     * This method deletes the data from the Database
     *
     * @param tableName The columnName of the table from which the data is to be
     * deleted
     * @param key The columnName of the key column, on which the row(s) are to
     * be deleted
     * @param id The value of the key column of the given table
     * @return Number of rows deleted
     * @throws SQLException if a database access error occurs, this method is
     * called on a closed Statement, the given SQL statement produces a
     * ResultSet object, the method is called on a PreparedStatement or
     * CallableStatement
     */
    public Integer deleteFromDatabase(String tableName, String key, Object id) throws SQLException {
        String query = String.format("DELETE FROM %s WHERE %s = '%s';", tableName, key, id);
        return this.executeUpdate(query); // Executing the Query
    }

    /**
     * Truncates a given table
     *
     * @param tableName name of the table to be truncated
     * @throws SQLException if a database access error occurs, this method is
     * called on a closed Statement, the given SQL statement produces a
     * ResultSet object, the method is called on a PreparedStatement or
     * CallableStatement
     */
    public void truncate(String tableName) throws SQLException {
        String query = String.format("TRUNCATE %s;", tableName);
        this.executeUpdate(query); // Executing the Query
    }

    /**
     * Disconnects all database objects (statements, resultSets etc...)
     * associated with this object from the database.
     */
//    public final void disconnect() {
//
//        List<Statement> connStatements = statements.get(this.uniqueId);
//        if (connStatements != null) {
//            for (Iterator<Statement> iterator = connStatements.iterator(); iterator.hasNext();) {
//                Statement statement = iterator.next();
//                try {
//                    statement.close();
//                } catch (SQLException ex) {
//                    StackTracer.printStackTrace(ex);
//                }
//
//                iterator.remove(); // remove current element from iterator and list
//            }
//            statements.remove(this.uniqueId);
//        }
//
//        List<ResultSet> connResults = resultSets.get(this.uniqueId);
//        if (connResults != null) {
//            for (Iterator<ResultSet> iterator = connResults.iterator(); iterator.hasNext();) {
//                ResultSet resultSet = iterator.next();
//                try {
//                    resultSet.close();
//                } catch (SQLException ex) {
//                    StackTracer.printStackTrace(ex);
//                }
//
//                iterator.remove(); // remove current element from iterator and list
//            }
//            resultSets.remove(this.uniqueId);
//        }
//
//        for (Iterator<Connection> it = connections.iterator(); it.hasNext();) {
//            Connection entry = it.next();
//            if (entry.equals(this.connection)) {
//                try {
//                    entry.close();
//                } catch (SQLException ex) {
//                    StackTracer.printStackTrace(ex);
//                }
//
//                it.remove(); // remove current element from iterator and list
//
//                break;
//            }
//        }
//    }
    /**
     * This method attempts to disconnect the given statement and resultSet from
     * the Database. It (optionally) attempts to disconnect the connection from
     * the database.
     *
     * @param resultSet the resultSet object to be closed
     * @param statement the statement object to be closed
     */
    public final void disconnect(ResultSet resultSet, Statement statement, boolean closeConnection) {

        this.disconnect(resultSet);

        this.disconnect(statement);

        if (closeConnection == true && connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                StackTracer.printStackTrace(ex);
            }
        }
    }

    /**
     * This method attempts to disconnect the given statement from the Database.
     *
     * @param statement the statement object to be closed
     */
    public final void disconnect(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                StackTracer.printStackTrace(ex);
            }
        }
    }

    /**
     * This method attempts to disconnect the given resultSet from the Database.
     *
     * @param resultSet the resultSet object to be closed
     */
    public final void disconnect(ResultSet resultSet) {
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException ex) {
                StackTracer.printStackTrace(ex);
            }
        }
    }

    /**
     * This method attempts to disconnect from the Database
     */
//    public final void disconnectAll() {
//
//        for (Map.Entry<Integer, List<Statement>> entry : statements.entrySet()) {
//            List<Statement> connStatements = entry.getValue();
//            for (Iterator<Statement> iterator = connStatements.iterator(); iterator.hasNext();) {
//                Statement statement = iterator.next();
//                try {
//                    statement.close();
//                } catch (SQLException ex) {
//                    StackTracer.printStackTrace(ex);
//                }
//
//                iterator.remove(); // remove current element from iterator and list
//            }
//
//            statements.remove(entry.getKey());
//        }
//
//        for (Map.Entry<Integer, List<ResultSet>> entry : resultSets.entrySet()) {
//            List<ResultSet> connResults = entry.getValue();
//            for (Iterator<ResultSet> iterator = connResults.iterator(); iterator.hasNext();) {
//                ResultSet resultSet = iterator.next();
//                try {
//                    resultSet.close();
//                } catch (SQLException ex) {
//                    StackTracer.printStackTrace(ex);
//                }
//
//                iterator.remove(); // remove current element from iterator and list
//            }
//
//            resultSets.remove(entry.getKey());
//        }
//
//        for (Iterator<Connection> it = connections.iterator(); it.hasNext();) {
//            Connection entry = it.next();
//            try {
//                entry.close();
//            } catch (SQLException ex) {
//                StackTracer.printStackTrace(ex);
//            }
//
//            it.remove(); // remove current element from iterator and list
//        }
//    }
}
