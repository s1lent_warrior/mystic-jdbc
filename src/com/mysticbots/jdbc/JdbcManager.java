/*
 * Copyright (C) 2016 MysticBots Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.mysticbots.jdbc;

import com.mysticbots.jdbc.model.IConnectionParameters;
import com.mysticbots.util.StackTracer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This is the Database utility class. This class should be extended by your
 * Class to use the functionality of {@code MysticJdbc}. This class do the
 * following tasks:<br>
 * <ol><li>Gets the Connection from DBConnection Class</li>
 * <li>Provides the querying tools to its child classes</li></ol>
 *
 * @author Sarfraz Ahmad
 * @author S1LENT W@RRIOR
 * @version 2.0
 *
 */
public class JdbcManager {

    /**
     * The MysticJdbc object. <br>This object acts as an interface b/w the
     * underlying Database and the layers above this Class
     */
    public MysticJdbc jdbcUtilities;

    /**
     * Gets the connection object
     *
     * @return The connection to the database
     */
    public Connection getConnection() {
        return jdbcUtilities.getConnection();
    }

    /**
     * The Constructor for JdbcUtilities class Instantiates some protected and
     * private fields of this class.
     *
     * @param jdbcParameters the connection parameters
     */
    public JdbcManager(Connection connection) {
        jdbcUtilities = new MysticJdbc(connection);
    }

    /**
     * The Constructor for JdbcUtilities class Instantiates some protected and
     * private fields of this class.
     *
     * @param jdbcParameters the connection parameters
     */
    public JdbcManager(IConnectionParameters jdbcParameters) {

        Connection connection = JdbcManager.openConnection(jdbcParameters);

        jdbcUtilities = new MysticJdbc(connection);
    }

    /**
     * Establishes the connection with the Database
     *
     */
    public static Connection openConnection(IConnectionParameters jdbcParameters) {
        Connection connection = null;
        try {
            // Establishing the connection with the database
            Class.forName(jdbcParameters.getDriverName());
            String connectionUrl = jdbcParameters.getConnectionString();
            String userName = jdbcParameters.getUsername();
            String userPass = jdbcParameters.getPassword();
            if (userName != null && !userName.isEmpty()) {
                connection = DriverManager.getConnection(connectionUrl, userName, userPass);
            } else {
                connection = DriverManager.getConnection(connectionUrl);
            }
//            System.out.println("******* Connection created successfully........" + connection);

        } catch (ClassNotFoundException | SQLException ex) {
            // In case of any SQLException
            StackTracer.printStackTrace(ex);
        }

        return connection;
    }

    /**
     * Disconnects the given statement and resultSet from the Database.
     *
     * @param resultSet the {@code ResultSet} object or null
     * @param statement the {@code Statement} object or null
     */
    public void disconnect(ResultSet resultSet, Statement statement) {
        this.jdbcUtilities.disconnect(resultSet, statement, true);
    }

    /**
     * Disconnects all database objects (statements, resultSets etc...)
     * associated with this object from the database.
     */
    public void disconnect() {
        this.jdbcUtilities.disconnect(null, null, true);
    }

}
