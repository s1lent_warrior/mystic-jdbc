**(UPDATE 23-April-2014)** We are glad to announce that our **API’s version 2.0** is finally available!

**New Features:**

*   get your Bean object directly from the _query_
*   get your Bean object directly from the _ResultSet_
*   get your Bean objects as a List or Map directly from the _query_
*   get your Bean objects as a List or Map directly from the _ResultSet_
*   100% backward compatible

**Other Changes:**

*   bundled jdbc 4 driver with the jar file (so don’t need to include one in your code!)
*   refactored com.swamtech.jdbc to com.softace.jdbc
*   bundled the javadoc inside the jar file (now you don’t need a separate javadoc.zip to include in your project)
*   updated the javadoc
*   modified the test code accordingly

A sample demonstration of one of our new features is given below:

```
// DegreeBean.java
@JdbcEntity(tableName = "degree")
public class DegreeBean {

    @JdbcAttribute(columnName = "degree_id", isKeyForMap = true)
    private int degreeId;
    @JdbcAttribute(columnName = "degree_title")
    private String degreeTitle;
    @JdbcAttribute(columnName = "degree_description")
    private String degreeDescription;

    /**
     * The default Constructor
     */
    public DegreeBean() {
    }

    // Getters and setters go here
}

    String query = "SELECT degree_id, degree_title, degree_description "
                + "FROM degree "
                + "WHERE degree_id = ?;";
    int degreeId = 1;

```

The DegreeBean can be retrieved in one of the following ways:

*   Getting DegreeBean directly from the query:

```
    DegreeBean degree = jdbcUtilities.getObjectFromQuery(query, DegreeBean.class, degreeId);

```

*   Getting DegreeBean from the ResultSet:

```
    ResultSet rs = this.jdbcUtilities.selectFromDatabase(query, degreeId);
    DegreeBean degree = jdbcUtilities.getObjectFromResultSet(rs, DegreeBean.class);

```

For more information check out the sample code.

**(UPDATE 13-April-2013)** Now **version 1.2** is available!

**New Features:**

*   get ResultSet in CSV format
*   save ResultSet to file in CSV
*   truncate table feature

**Bug fixes:**

*   delete table bug fixed

**- Other Changes:**

*   improved javadoc
*   updated the javadoc
*   modified the test code accordingly
*   renamed JdbcUtility class to JdbcUtilities

This is the **Java JDBC Utility API** to conveniently perform mysql-jdbc operations.

This API is developed to perform MySQL-JDBC operations in a convenient way.

e.g: In order to perform a single SELECT query on a database, there are a lot of steps involved like:

*   loading the JDBC driver
*   establishing the connection with the database
*   perparing the statement (PreparedStatement)
*   and finally executing the query returns the resultset!

However, this API not only reduces these steps but also allows you to prepare your statement in a natural way!

A comparison between the PreparedStatement and our alternative to the prepared statement is given below:

*   **Using a PreparedStatement:**

```
    String query = "INSERT INTO student(student_id, name, age, class, year) 
                   + VALUES(?, ?, ?, ?, ?);";
    PreparedStatement preparedStatement = dbConnection.prepareStatement(query);
    preparedStatement.setInt(1, 11);
    preparedStatement.setString(2, "Ali");
    preparedStatement.setInt(3, 20);
    preparedStatement.setString(4, "O-Level");
    preparedStatement.setInt(3, 2013);
    preparedStatement .executeUpdate();

```

*   **Performing same query Using MySQL-JDBC-Utilities:**

```
    String query = "INSERT INTO student(student_id, name, age, class, year) 
                   + VALUES(?, ?, ?, ?, ?);";
    jdbcUtility.executeInsert(11, "Ali", 20, "O-Level", 2013);

```

Some of its key features include:

*   One time JDBC configuration
*   No need to setup Database environment multiple times
*   Perform MySQL DML (insert, update, delete, select) operations efficiently
*   Easily retrieve the ResultSet Metadata (rows count, column names etc…)
*   Compatible with J2SE 5 and higher versions
*   Compatible with MySQL 5.x versions
*   Compatible with JDBC 4+ driver
*   Javadoc and example code included
*   No special setup needed, just add this jar into your classpath and start developing your apps
*   and many more

Got any suggestions, ideas or critique?

Everyone is welcome to give suggestions and ideas.

Feel free to contact me at: [silent.warrior.muneeb@gmail.com](mailto:silent.warrior.muneeb@gmail.com)
